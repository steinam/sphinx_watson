#Treiber eingebunden
[void][reflection.assembly]::loadfrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.8\Assemblies\v2.0\MySql.Data.dll"); 

#Aufbau einer Connection
#definiert durch Username, Passwort, Server, Db, Port
$constring = "Server=localhost;UID = watson;Pwd=ganzgeheim;database=watson";

#Connection-Object
$con = New-Object MySql.Data.MySqlClient.MySqlConnection;
$con.ConnectionString = $constring

#Implementierung eines DataAdapters für MySQL
$SqlDataAdapter = New-Object MySql.Data.MySqlClient.MysqlDataAdapter;

#virtuelle datenbank auf der .NET-Framework-Seite
$DataSet = New-Object System.Data.DataSet



function getAllReport()
{
	$sql = "select * from tbl_report;"

	#connection aufmachen
	$con.open()

	$command = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$command.CommandText = $sql
	$command.Connection = $con
	
	$SqlDataAdapter.SelectCommand = $command;
	#Inhalt des dataSets werden gelöscht
	$DataSet.Reset()
	
	#SQlDataAdapter füllt die Daten in das DataSet
	$SqlDataAdapter.Fill($DataSet);
	
	
	$con.close()











}












function getUsers()
{
	#Array zum Speichern der User und Rückgabe an den Controler
	$userliste = @()
	
	#Sql-Statement erzeugen
	$sql = "select Anmeldename from tbl_user;"
	$command = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$command.CommandText = $sql
	$command.Connection = $con
	
	Write-Debug "Verbindung offen"
	$con.Open()
	
	$reader = $command.ExecuteReader()
	
	
	
	#Schleife über dataReader
	while($reader.Read())
	{
		$userliste += $reader["Anmeldename"]
	}
	
	$con.Close()
	
	return $userliste
	
}



