Glossar
========

.. glossary::


   Command
   	Repräsentiert im .NET-Framework den SQL-Befehl, der auf eine Datenbank abgegeben wird. Er braucht dazu noch mindestens ein Connection-Objekt
   	
   	.. code-block:: sh
   	
   		$cmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
		$cmd.CommandText = $query 
		$results = $cmd.ExecuteReader()


   Connection
   	Objekt des .NET-Frameworks, welches die Verbindungsinformationen zu einer Datenbank aufnimmt. Er hat häufig eine genau definierte Syntax. Dazu gehören  Username/Passwort/Hostname/Datenbankname/Port
   	
   	.. code-block:: sh
   	
   		$connstring = "Server=localhost;Uid=root;Pwd='XXXXX';Database=XXXXXX";
		$con = New-Object Mysql.Data.MysqlClient.MysqlConnection;
		$con.ConnectionString = $connstring;

   DataReader
   	.NET-Klasse, die Daten eines Command-Objektes aufnimmt. Sie muss über eine Schleife in eine andere Datenstruktur, meist ein Array, übertragen werden, das sie nur forward-only zu lesen ist.   
   
   PrimalForms
   	Desktop-Programm der Firma Sapien, welches auf einfache Weise GUI-Oberfl#ächen (WinForm) für Powershell erstellt. Leider nicht mehr erhältlich.
   	
   Provider
  	.NET-Bezeichnung für einen spezifischen Datenbanktreiber eines Herstellers 
  	
   Refaktorisierung
   	Umwandlung bestehenden Quellcodes zur
   	- besseren Verständlichkeit
   	- besseren Weiterverwendbarkeit
  	
  	
   
  