﻿#Basispfad des Active Directory

$domain = get-AdDomain
$DomainPfad = $domain.DistinguishedName

$baseDn = "OU=Schule,"+ $domainPfad


#Schülerliste einlesen

Write-Host "Einlesen der CSV-Datei"

$csvlist = Import-Csv C:\temp\schule.csv -Delimiter ","

Write-Host $csvlist

#Funktionen zur AD-Erstellung

## OU Erstellung

function Set-OUs

{ 
    Write-Host "Erstellung der Organisationseinheiten"

    New-ADOrganizationalUnit -Name "Schule" -Path $domainpfad -ProtectedFromAccidentalDeletion $false

    New-ADOrganizationalUnit -Name "Schueler" -Path $baseDn -ProtectedFromAccidentalDeletion $false

        foreach ($schueler in $csvlist)
        {

        $OUPfad ="OU=Schueler,"+$baseDn

        $OUs=Get-ADOrganizationalUnit -Filter *

            if ($OUs.Name -notcontains $schueler.Klasse)
            {

            New-ADOrganizationalUnit -Name $schueler.Klasse -Path $OUPfad  -ProtectedFromAccidentalDeletion $false

            }

        }
 Write-Host "Erstellung der Organisationseinheiten abgeschlossen"

}

## Benutzererstellung
  
function Set-Users

{
    Write-Host "Erstellung der User"


        foreach ($schueler in $csvlist)

        {


        $HomeOrdner = "C:\HomeDirs\"+$schueler.Beruf +"\"+$schueler.Klasse +"\"+$schueler.LoginName

        $passwort = $schueler.Passwort | ConvertTo-SecureString -AsPlainText -Force

        $Userpfad = "OU="+$schueler.klasse+",OU=Schueler,"+$baseDn

        New-ADUser -Path $Userpfad -Enabled $True -Name $schueler.Name -SamAccountName $schueler.LoginName -EmailAddress $schueler.Email -AccountPassword $passwort -CannotChangePassword $true -PasswordNeverExpires $true -HomeDrive "H:" -HomeDirectory $HomeOrdner  

        }

    Write-Host "Erstellung der User erledigt"

}

## Gruppenerstellung

function Set-Groups

{
    Write-Host "Erstellung der Gruppen"

    New-ADOrganizationalUnit -Name "GrpSchueler" -Path $basedn -ProtectedFromAccidentalDeletion $false

    $Gruppenpfad = "OU=GrpSchueler,"+$baseDn

        foreach ($schueler in $csvlist)

        {
                             
            if ($schueler.Gruppe1 -ne "")

            {
            $Groups= Get-ADGroup -Filter *
            if ($Groups.Name -notcontains $schueler.Gruppe1)
            {
            New-ADGroup -Path $Gruppenpfad -Name $schueler.Gruppe1 -GroupScope Global
            }
            }

            if ($schueler.Gruppe2 -ne "")
            {
            $Groups= Get-ADGroup -Filter *
            if ($Groups.Name -notcontains $schueler.Gruppe2)
            {
            New-ADGroup -Path $Gruppenpfad -Name $schueler.Gruppe2 -GroupScope Global
            }
            }

            if ($schueler.Gruppe3 -ne "")

            {
            $Groups= Get-ADGroup -Filter *
            if ($Groups.Name -notcontains $schueler.Gruppe3)
            {
            New-ADGroup -Path $Gruppenpfad -Name $schueler.Gruppe3 -GroupScope Global
            }
            }
        }

    Write-Host "Erstellung der Gruppen erledigt"

}

## Gruppenzuweisung

function UsertoGroups

{
    Write-Host "Gruppenzuweisung der User"

        foreach ($schueler in $csvlist)

        {

        $myGroups = @()


        $myGroups += $schueler.Gruppe1
        if($schueler.Gruppe2 -ne "")
        {
            $myGroups += $schueler.Gruppe2
        }

        if($schueler.Gruppe3 -ne "")
        {
             $myGroups += $schueler.Gruppe3
        }

            foreach($gruppe in $myGroups)
            {
             Add-ADGroupMember $gruppe $schueler.LoginName
            }

        }

    Write-Host "Gruppenzuweisung der User erledigt"

}

## Erstellen der Homeverzeichnisse

function Set-Homeverz

{

    Write-Host "Erstellen der Homeverzeichnisse"
        
        foreach ($schueler in $csvlist)
        {
        $HomeOrdner = "C:\HomeDirs\"+$schueler.Beruf +"\"+$schueler.Klasse +"\"+$schueler.LoginNameName
                     
        mkdir $HomeOrdner

        $Zugriff = Get-ACL -Path $HomeOrdner
        $User = get-AdUser $schueler.LoginName

        $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

        $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
        $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
        $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


        $Regel = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
        $Zugriff.AddAccessRule($Regel)
   
        Set-ACL -Path $HomeOrdner -AclObject $Zugriff
        }

    Write-Host "Erstellen der Homeverzeichnisse erledigt"
}

#Löschen aller Einträge

function MakeClean

{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\HomeDirs -Recurse     

    Write-Host "Einträge gelöscht"  

}


##Aufrufen der Funktionen

Set-OUs

Set-Users

Set-Groups

UsertoGroups

Set-Homeverz
