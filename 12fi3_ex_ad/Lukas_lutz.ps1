﻿
cls



$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



$baseDn = "OU=Schueler,"+ $domainRoot



$OU_HList = @{} 
$OU_UList = @{} 
$Group_List= @{}





$csvresult = import-csv C:\temp\schule.csv -Delimiter ","

write-Host $csvresult




function setOUs{


    begin{
    Write-Host "lege OUs an"
    New-ADOrganizationalUnit -Name "Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
        
    }

  
    process{
        

     

        $Ou_Name = $_.Klasse
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        if($_.Beruf -ne "")
        {
            $OU_DN_PATH = "OU="+$_.Klasse+"," + $baseDn
            $Ou_Name = $_.Beruf

            $OU_UList[$OU_Name]= $OU_DN_Path
 
        }


    }



    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs hoffentlich angelegt"    
    }
}





function setGroups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        if($_.Gruppe2 -ne "")
        {
            $Group_List[$_.Gruppe2]=$_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
            $Group_List[$_.Gruppe3]=$_.Gruppe3
        }
    }

    end{   
        
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}


function  setUser
{
	begin{
	    Write-Debug "Adding User"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Klasse +"," + $baseDN	

        if($_.Beruf -ne "")
        {
            $OU_DN_PATH = "OU="+$_.Klasse+"," + $baseDn
     

           


            $_.DName = "OU=" +$_.Beruf +"," + $OU_DN_PATH    
        }


        



        $HomeDir = "C:\homeDir\"+$_.Beruf
        
        
        $homeDir2 = "$HomeDir\"+$_.Klasse
        $HomeDir3 = "$HomeDir2\"+$_.Name

        Write-Host $HomeDir3

        $password = "Test!!1234" | ConvertTo-SecureString -AsPlainText -Force

   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $false -HomeDrive "H:" -HomeDirectory $HomeDir3 -EmailAddress $_.Email	    

        



}
	
	end{
	    write-debug "finished adding user"
	}
	
}



function setUserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName
         }

    }

    end{
    }
}


function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\homeDirs\"
        mkdir $baseHomeDir
       

    }

    process{
       
       # baseHomedir Pfad erweitern 
        $homefolder = $baseHomeDir+$_.Beruf
        $homefolder2 = "$homefolder\"+$_.Klasse
        $homefolder3 = "$homefolder2\"+$_.Name



        mkdir $homefolder3
        
       
        
    $ACL = Get-ACL -Path "$Homefolder3"
    $User = get-AdUser $_.Name

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"


    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "$Homefolder3" -AclObject $ACL



    }

    end{


    }



}



function remove-AllOUsAndGroups
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\homeDirs -Recurse     
   
}




#######Ablaufsteuerung########








remove-AllOUsAndGroups
$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups
$csvresult | set-HomeDir



#######################
#Lukas Lutz 18.01.2018#
#######################
















