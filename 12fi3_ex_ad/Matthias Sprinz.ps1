﻿cls



$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName

$baseDn = "OU=intra.maddias,"+ $domainRoot


#Aufnahme der Haupt-OUs
$OU_HList = @{} 
# Aufnahme der Unter-OUs
$OU_UList = @{} 
$Group_List= @{}


## csv-Datei einlesen
$csvresult = import-csv C:\temp\schule.csv -Delimiter ","

write-Host $csvresult



#OU Schueler wird angelegt umd Klassen anzulegen:

function setOUs{

    
    begin{
    Write-Host "lege OUs an"
    New-ADOrganizationalUnit -Name "Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
        
    }

    
    process{
        
        
     

        $Ou_Name = $_.HauptOU
        $OU_DN_Path = "OU=intra.maddias,Schueler," + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        if($_.UnterOU -ne "Klasse")
        {
            $OU_DN_PATH = "OU=Klasse"+$_.HauptOU+"," + $baseDn
            $Ou_Name = $_.UnterOU

            $OU_UList[$OU_Name]= $OU_DN_Path
 
        }


    }
        end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs wurden angelegt"    
    }
}



#Gruppen werden angelegt um Schuelern Klassen zuordnen zu können.


function setGroups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "Gruppen" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        if($_.Gruppe2 -ne "10FI1")
        {
            $Group_List[$_.Gruppe2]=$_.Gruppe2
        }

        if($_.Gruppe3 -ne "10FI1")
        {
            $Group_List[$_.Gruppe3]=$_.Gruppe3
        }
    }

    end{   
        
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=Gruppen, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}



#Die Schueler/User werden angelegt: (und Passwort darf nicht geändert werden)


function  setUser
{
	begin{
	    Write-Debug "User wird angelegt"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU=GrpSchueler"+ $_.HauptOU +"," + $baseDN	

        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU=User"+$_.HauptOU+"," + $baseDn
          

           


            $_.DName = "OU=Benutzer" +$_.UnterOU +"," + $OU_DN_PATH    
        }


        


    	$HomeDir = "C:\home\"+$_.Anmeldename
        Write-Host $HomeDir

        $password = "Sommer!1234" | ConvertTo-SecureString -AsPlainText -Force
	    
          #User wird angelegt und Passwort kann nach einmaliger Änderung in der Erstanmeldung nicht gändert werden:
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $true -HomeDrive "H:" -HomeDirectory $HomeDir SetAduser -CannotChangePassword	    

        



}
	
	end{
	    write-debug "User wurde hinzugefügt"
	}
	
}



#Nachdem Gruppen und User erstellt worden sind können die User nun den Klassen/Gruppen zugeordnet werden

function setUserToGroups
{
    begin{
        Write-Host "Ordne Schueler den Klassen zu"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        if($_.Gruppe -ne "Schueler")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Globale_Gruppe3

        }
#
        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "Schueler")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Globale_Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.Anmeldename 
         }

    }

    end{
    }
}




#Den Usern wird ein Homelaufwerk mit Ordnerstruktur erstellt

function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\home\"
        mkdir $baseHomeDir
       

    }

    process{
       
        $homefolder = $baseHomeDir+$_.Anmeldename

        mkdir $homefolder
        
       
        
    $ACL = Get-ACL -Path "C:\Home\$($_.Anmeldename)"
    $User = get-AdUser $_.Anmeldename

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\Home\$($_.Anmeldename)" -AclObject $ACL


    }

    end{


    }



}




#MakeClean:
function remove-AllOUsAndGroups
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\home -Recurse     
   
}

#Ablaufsteuerung:
#        Pipe    Funktion
remove-AllOUsAndGroups
$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups
$csvresult | set-HomeDir