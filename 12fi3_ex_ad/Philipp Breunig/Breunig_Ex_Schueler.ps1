# PHILIPP BREUNIG



cls

# globale Variablen


#OUs und Gruppen werden angelegt
#User anlegen und Gruppen zuweisen

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



#Hashtable als Struktur f�r OUs
$OU_Klassen = @{}
$Group_List= @{}
$Berufsliste = @{}




## csv-Datei einlesen
$csvresult = import-csv C:\Windows\temp\schule.csv -Delimiter ","




#Funktionen

function MakeClean{

     Remove-ADOrganizationalUnit -Identity "OU=Schueler,DC=ADPS10,DC=local" -Confirm:$False -recursive:$True 
     Remove-ADOrganizationalUnit -Identity "OU=GrpSchueler,DC=ADPS10,DC=local" -Confirm:$False -recursive:$True 
           
     rmdir C:\HomeDirs\ -Recurse     
   
}

MakeClean


function Create-OUs {
    
    begin{
    # Anlage einer �berOU zum leichteren L�schen
        #New-ADOrganizationalUnit -Name "Ex" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False

        #Anlage OU Schueler
        New-ADOrganizationalUnit -Name "Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False

        #Anlage OU GrpSchueler
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $Domainroot -ProtectedFromAccidentalDeletion $False

    }

    process{
        
        $OU_Name = $_.Klasse
        $OU_DN_Path = ""+"OU=Schueler,"+$DomainRoot
        $OU_Klassen[$OU_Name]=$OU_DN_Path

    }

    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_Klassen.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_Klassen[$element] -ProtectedFromAccidentalDeletion $False
        
        }     
        
        Write-Host "OUs angelegt"    
    }


}

$csvresult | Create-OUs


function Create-Groups {
    begin{
    
        $Group_List.Clear()
        
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        $Group_List[$_.Gruppe2]=$_.Gruppe2

        $Group_List[$_.Gruppe3]=$_.Gruppe3
    }

    end{   
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler," + $DomainRoot
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}

$csvresult | Create-Groups


function Create-User{
	begin{
	    Write-Host "User werden angelegt."
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Klasse +"," + "OU=Schueler," + $DomainRoot

    	$HomeDir = "C:\HomeDirs\$_.Beruf\$_.Klasse\$_.LoginName"
        Write-Host $HomeDir

        $password = $_.Passwort | ConvertTo-SecureString -AsPlainText -Force
#       New-ADUser -Path $_.DName -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword (ConvertTo-SecureString -force -AsPlainText "Passw0rd") -ChangePasswordAtLogon $true	    
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -PasswordNeverExpires $True -CannotChangePassword $True -HomeDrive "H:" -HomeDirectory $HomeDir

}
	
	end{
	    write-host "User angelegt."
	}
	
}

$csvresult | Create-User

function Gruppenzuordnung {
    begin{
        Write-Host "User erhalten Gruppen."

    }

    process{

        $myGroups = @()
        $myGroups += $_.Gruppe1
        $myGroups += $_.Gruppe2
        $myGroups += $_.Gruppe3


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName 
         }

    }

    end{
        Write-Host "Gruppen wurden zugeordnet."
    }
}

$csvresult | Gruppenzuordnung


function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\HomeDirs\"
        mkdir $baseHomeDir
        
    }

    process{
        
        $Beruf = $_.Beruf
        $Klasse = $_.Klasse
        $Ordnerpfad = "C:\HomeDirs\$Beruf\$Klasse\"

        Try{
        mkdir $Ordnerpfad
        }
        Catch{}


        $homefolder = $Ordnerpfad+$_.LoginName

        mkdir $homefolder
        
       
    $ACL = Get-ACL -Path "$Ordnerpfad\$($_.LoginName)"
    $User = get-AdUser $_.LoginName

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    #$groupsOrUser = [System.Security.Principal.NTAccount]"\\steinam\Mahn$"
    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\Home\$($_.Anmeldename)" -AclObject $ACL

    }

    end{


    }



}

$csvresult | set-HomeDir




#######Ablaufsteuerung########






















