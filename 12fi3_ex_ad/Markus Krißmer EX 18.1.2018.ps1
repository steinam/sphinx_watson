﻿## Markus Krißmer 12FI3 ##

cls

# globale Variablen
#$DebugPreference = Continue


#OUs und Gruppen werden angelegt


$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



$baseDn = "OU=OU_Schueler,"+ $domainRoot


#Hashtable als Struktur für OUs

$OU_HList = @{} 
$OU_UList = @{} 
$Group_List= @{}


## csv-Datei einlesen

$csvresult = import-csv C:\temp\schule.csv -Delimiter ","

write-Host $csvresult


#Farbe Grün Ou's anlegen

function setOUs{

    begin{
    Write-Host "OUs anlegen"
    New-ADOrganizationalUnit -Name "OU_Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
        
    }

    process{
         

        $Ou_Name = $_.Klasse
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path
    }

    
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
      
         
        Write-Host "OUs angelegt"    
    }
}

# Farbe Rot Gruppen anlegen

function setGroups
{
    begin{
        Write-Host "Gruppen anlegen"
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        if($_.Gruppe2 -ne "")
        {
            $Group_List[$_.Gruppe2]=$_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
            $Group_List[$_.Gruppe3]=$_.Gruppe3
        }
    }

    end{   
        
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
        Write-Host "Gruppen wurden angelegt"
    }
}


# Farbe Lila User anlegen

#User anlegen

function  setUser
{
	begin{
	    Write-Host "User werden angelegt"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Klasse +"," + $baseDN	


        

    	$HomeDir = "C:\Home\"+$_.LoginName
        Write-Host $HomeDir

# Neues Passwort wegen "Das Kennwort entspricht nicht den Domänenanforderungen bezüglich 
#Länge, Komplexität und Verlauf."

        $password = "Sommer2016" | ConvertTo-SecureString -AsPlainText -Force	    
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $false -HomeDrive "H:" -HomeDirectory $HomeDir	    

        
}
	
	end{
	    write-host "User wurden angelegt"
	}
	
}

# Farbe Gelb User Gruppen zuweisen

function setUserToGroups
{
    begin{
        Write-Host "User zu Gruppen ordnen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName 
         }

    }

    end{
    Write-Host "User wurden zugewiesen"
    }
    
}


#Farbe Blau Homelaufwerke generieren

function set-HomeDir{

    begin{
            Write-Host "Laufwerke anlegen"

        $baseHomeDir = "C:\HomeDirs\"
        mkdir $baseHomeDir
       

    }

    process{
       
        $homefolder = $baseHomeDir+$_.LoginName

        mkdir $homefolder
        
       
        
    $ACL = Get-ACL -Path "C:\HomeDirs\$($_.LoginName)"
    $User = get-AdUser $_.LoginName

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

   
    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\HomeDirs\$($_.LoginName)" -AclObject $ACL

    }

    end{
    Write-Host "Laufwerke wurden angelegt"
    }
    
}


#######Ablaufsteuerung########


$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups
$csvresult | set-HomeDir





# MakeClean Methode


function remove-AllOUsAndGroups
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\home -Recurse     
   
}


remove-AllOUsAndGroups