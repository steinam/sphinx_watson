cls

# globale Variablen
#$DebugPreference = Continue


#OUs und Gruppen werden angelegt
#User anlegen und Gruppen zuweisen

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



$baseDn = "OU=OU_Schueler,"+ $domainRoot
#$baseDn = "datteln.local"

#Hashtable als Struktur f�r OUs
#Key = OUName, Value = DC=njd
$OU_HList = @{} #Aufnahme der Haupt-OUs
$OU_UList = @{} # Aufnahme der Unter-OUs
$Group_List= @{}




## csv-Datei einlesen
$csvresult = import-csv C:\temp\schule.csv -Delimiter ","

write-Host $csvresult



## diverse Funktionen
function setOUs{

    #Vor Beginn der Pipe
    begin{
    Write-Host "lege OUs an"
    New-ADOrganizationalUnit -Name "OU_Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
        
    }

    #F�r jede Zeile in der Pipe
    process{
        
         #Objekte der Pipeline erhalten eine neue Eigenschaft
         #DistinguishedName 
     

        $Ou_Name = $_.Beruf
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        if($_.Klasse -ne "")
        {
            $OU_DN_PATH = "OU="+$_.Beruf+"," + $baseDn
            $Ou_Name = $_.Klasse

            $OU_UList[$OU_Name]= $OU_DN_Path
 
        }


    }


    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs angelegt"    
    }
}





function setGroups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        if($_.Gruppe2 -ne "")
        {
            $Group_List[$_.Gruppe2]=$_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
            $Group_List[$_.Gruppe3]=$_.Gruppe3
        }
    }

    end{   
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler, " + $DomainRoot
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}


function  setUser
{
	begin{
	    Write-Debug "Adding User"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Beruf +"," + $baseDN	

        if($_.Klasse -ne "")
        {
            $OU_DN_PATH = "OU="+$_.Beruf+"," + $baseDn
          #  $Ou_Name = $_.Klasse

           


            $_.DName = "OU=" +$_.Klasse +"," + $OU_DN_PATH    
        }


        


    	$HomeDir = "C:\HomeDirs\$($_.Beruf)\$($_.Klasse)\$($_.LoginName)"
    
        Write-Host $HomeDir

        $password = $_.Passwort | ConvertTo-SecureString -AsPlainText -Force 	  
#       New-ADUser -Path $_.DName -SamAccountName $_.LoginName -Name $_.Name -AccountPassword (ConvertTo-SecureString -force -AsPlainText "Passw0rd") -ChangePasswordAtLogon $true	    
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $false -HomeDrive "H:" -HomeDirectory $HomeDir -EmailAddress $_.Email	    

        



}
	
	end{
	    write-debug "finished adding user"
	}
	
}



function setUserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName
         }

    }

    end{
    }
}


function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\HomeDirs\"
        mkdir $baseHomeDir
       

    }

    process{
       
        $homefolder = $baseHomeDir+$_.Beruf+$_.Klasse+$_.LoginName

        mkdir $homefolder
        
       
    #$ACL = Get-ACL -Path "C:\HomeDirs\$($_.Beruf)\$($_.Klasse)\$($_.LoginName)"   
    $ACL = Get-ACL -Path $homefolder
    $User = get-AdUser $_.LoginName

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    #$groupsOrUser = [System.Security.Principal.NTAccount]"\\steinam\Mahn$"
    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path $homefolder -AclObject $ACL

#    New-SmbShare "C:\HomeDirs\$($_.Beruf)\$($_.Klasse)\$($_.LoginName)" -Name Home -FullAccess $_.LoginName

    }

    end{


    }



}



function MakeClean
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True  
     Remove-ADOrganizationalUnit -Identity "GrpSchueler" -Confirm:$False -recursive:$True  
     rmdir "C:\HomeDirs" -Recurse   
     Write-Host "Zur�ckgesetzt" 
   
}




#######Ablaufsteuerung########







##        Pipe    Funktion

## AD leeren
MakeClean

#AD wiederherstellen
$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups
$csvresult | set-HomeDir



















