##### 2. Stegreifaufgabe AWP - 12FI3 - 18.01.2018 - Jonas Peter #####

cls

# globale Variablen
#$DebugPreference = Continue


#OUs und Gruppen werden angelegt
#User anlegen und Gruppen zuweisen

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName

$baseDn = $domainRoot

#Variablen zum l�schen der OUs
$delOUs1 = "OU=Schueler,"+ $domainRoot
$delOUs2 = "OU=GrpSchueler,"+ $domainRoot

$OU_HList = @{} #Aufnahme der Haupt-OUs
$OU_UList = @{} # Aufnahme der Unter-OUs
$Group_List= @{}




## csv-Datei einlesen
$csvresult = import-csv C:\temp\schule.csv -Delimiter ","

write-Host $csvresult



## diverse Funktionen
function setOUs{

    #Vor Beginn der Pipe
    begin{
    Write-Host "lege OUs an"
        
    }

    #F�r jede Zeile in der Pipe
    process{
        
         #Objekte der Pipeline erhalten eine neue Eigenschaft
         #DistinguishedName 
     

        $Ou_Name = $_.Gruppe1
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        if($_.Klasse -ne "")
        {
            $OU_DN_PATH = "OU="+$_.Gruppe1+"," + $baseDn
            $Ou_Name = $_.Klasse

            $OU_UList[$OU_Name]= $OU_DN_Path
 
        }


    }


    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs hoffentlich angelegt"    
    }
}





function setGroups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        if($_.Gruppe2 -ne "")
        {
            $Group_List[$_.Gruppe2]=$_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
            $Group_List[$_.Gruppe3]=$_.Gruppe3
        }
    }

    end{   
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}


function  setUser
{
	begin{
	    Write-Debug "Adding User"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Gruppe1 +"," + $baseDN	

        if($_.Klasse -ne "")
        {
            $OU_DN_PATH = "OU="+$_.Gruppe1+"," + $baseDn

           


            $_.DName = "OU=" +$_.Klasse +"," + $OU_DN_PATH    
        }


        


    	$HomeDir = "C:\HomeDirs\"+$_.Beruf+"\"+$_.Klasse+"\"+$_.LoginName
        Write-Host $HomeDir

        $password = $_.Passwort | ConvertTo-SecureString -AsPlainText -Force
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -EmailAddress $_.Email -Company $_.Klasse -Department $_.Beruf -AccountPassword $password -CannotChangePassword $true -HomeDrive "H:" -HomeDirectory $HomeDir	    

        



}
	
	end{
	    write-debug "finished adding user"
	}
	
}



function setUserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName 
         }

    }

    end{
    }
}


function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\HomeDirs\"
        mkdir $baseHomeDir
       

    }

    process{
       
        $homefolder = $baseHomeDir+$_.Beruf+"\"+$_.Klasse+"\"+$_.LoginName

        mkdir $homefolder
        
       
        
    $ACL = Get-ACL -Path "C:\HomeDirs\$($_.Beruf+"\"+$_.Klasse+"\"+$_.LoginName)"
    $User = get-AdUser $_.LoginName

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\HomeDirs\$($_.Beruf+"\"+$_.Klasse+"\"+$_.LoginName)" -AclObject $ACL


    }

    end{


    }



}



function MakeClean
{

     Remove-ADOrganizationalUnit -Identity $delOUs1 -Confirm:$False -recursive:$True
     Remove-ADOrganizationalUnit -Identity $delOUs2 -Confirm:$False -recursive:$True    
     rmdir C:\HomeDirs -Recurse     
   
}




#######Ablaufsteuerung########







#        Pipe    Funktion
MakeClean
$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups
$csvresult | set-HomeDir