﻿##### 2. Stegreifaufgabe / 18.01.18 / Henning Zirkelbach #####


cls

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName


$baseDn = $domainRoot

#Hashtable als Struktur für OUs
#Key = OUName, Value = DC=njd
$OU_HList = @{} #Aufnahme der Haupt-OUs
$OU_UList = @{} # Aufnahme der Unter-OUs
$Group_List= @{}




## csv-Datei einlesen
$csvresult = import-csv C:\Users\Administrator\Desktop\TMP\schule.csv -Delimiter ","

#write-Host $csvresult



## diverse Funktionen
function setOUs{

    #Vor Beginn der Pipe
    begin{
    Write-host "Lege OUs an"
    New-ADOrganizationalUnit -Name "Schueler" -Path $baseDn -ProtectedFromAccidentalDeletion $False
        
    }

    #Für jede Zeile in der Pipe
    process{
        
         #Objekte der Pipeline erhalten eine neue Eigenschaft
         #DistinguishedName 
     

        $Ou_Name = $_.Klasse
        $OU_DN_Path = "OU=Schueler," + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        


    }


    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        Write-host "OUs angelegt"    
    }
}





function setGroups
{
    begin{
        write-host "Lege Gruppen an"
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1
        $Group_List[$_.Gruppe2]=$_.Gruppe2
        $Group_List[$_.Gruppe3]=$_.Gruppe3
       
    }

    end{   
        
        # write-host $Group_List

        foreach($element in $Group_List.Keys)
        {
            $path = "OU=GRPSchueler, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }

        Write-host "Gruppen angelegt"
    }
}

# Erstelle User + HomeDirs
function  setUser
{
	begin{

        Write-host "Lege User an"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Klasse +",OU=Schueler," + $baseDn	


        

    	$HomeDir = "C:\HomeDirs\" + $_.Beruf + "\" + $_.Klasse + "\" + $_.Name
        #Write-Host $HomeDir

        $password = $_.Passwort | ConvertTo-SecureString -AsPlainText -Force

   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -PasswordNeverExpires $true -CannotChangePassword $true -EmailAddress $_.Email -HomeDrive "H:" -HomeDirectory $HomeDir	    

        


        ######################################## HOME DIRS ########################################

        
        mkdir $HomeDir
        
       
        
        $ACL = Get-ACL -Path $HomeDir
        $User = get-AdUser $_.LoginName

        $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"
        $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
        $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
        $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


        $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
        $acl.AddAccessRule($accessrule)
   
        Set-ACL -Path $HomeDir -AclObject $ACL

        ###########################################################################################





}
	
	end{
	    write-host "User angelegt"
	}
	
}



function setUserToGroups
{
    begin{
        Write-host "Füge User zu Gruppen hinzu"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        $myGroups += $_.Gruppe2
        $myGroups += $_.Gruppe3


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName
         }

    }

    end{
        Write-host "User zu Gruppen hinzugefügt"
     
    }
}






function MakeClean
{

     Remove-ADOrganizationalUnit -Identity ("OU=Schueler," + $baseDn) -Confirm:$False -recursive:$True    
     Remove-ADOrganizationalUnit -Identity ("OU=GrpSchueler," + $baseDn) -Confirm:$False -recursive:$True    
     rm C:\HomeDirs\ -Recurse     
   
}




#######Ablaufsteuerung########







#        Pipe    Funktion
MakeClean
$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups



















