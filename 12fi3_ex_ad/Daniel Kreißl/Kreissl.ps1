﻿# globale Variablen
#$DebugPreference = Continue


#OUs und Gruppen werden angelegt
#User anlegen und Gruppen zuweisen

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



$baseDn = "OU=Schueler"+ "," + $domainRoot


#Hashtable als Struktur für OUs

$OU_HList = @{} #Aufnahme der Haupt-OUs
$OU_UList = @{} # Aufnahme der Unter-OUs
$Group_List= @{}


## csv-Datei einlesen
$csvresult = import-csv C:\windows\temp\schule.csv -Delimiter ","

write-Host $csvresult




## diverse Funktionen
function setOUs{

    #Vor Beginn der Pipe
    begin{
    Write-Host "lege OUs an"
    New-ADOrganizationalUnit -Name "Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
        
    }

    #Für jede Zeile in der Pipe
    process{
        
         #Objekte der Pipeline erhalten eine neue Eigenschaft
        
     

        $Ou_Name = $_.Beruf
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        #if($_.UnterOU -ne "")
        #{
        #    $OU_DN_PATH = "OU="+","+$_.HauptOU+"," + $baseDn
        #    $Ou_Name = $_.UnterOU
#
 #           $OU_UList[$OU_Name]= $OU_DN_Path
 #
  #      }


    }


    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs hoffentlich angelegt"    
    }
}

function setGroups
{
    begin{
        
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDn -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Gruppe1]=$_.Gruppe1

        if($_.Globale_Gruppe2 -ne "")
        {
            $Group_List[$_.Gruppe2]=$_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
            $Group_List[$_.Gruppe3]=$_.Gruppe3
        }
    }

    end{   
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}



function  setUser{
    begin{
	    Write-Debug "Adding User"
	}

    process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.Beruf +"," + $baseDN	

        #if($_.UnterOU -ne "")
        #{
        #    $OU_DN_PATH = "OU="+$_.Beruf+"," + $baseDn
   # 
    #        $_.DName = "OU=" +$_.UnterOU +"," + $OU_DN_PATH    
     #   }
    

        $password = $_.Passwort | ConvertTo-SecureString -AsPlainText -Force
        #Passwort soll nicht geändert werden nicht in den Eigenschaften nicht gefunden.
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -HomeDrive "H:" -HomeDirectory $HomeFolder -PasswordNeverExpires $true
}

    end{
	    write-debug "finished adding user"
	}
	



}

	

function setUserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "")
        {
            $myGroups += $_.Gruppe2
        }

        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.Anmeldename 
         }

    }

    end{
    }
}

        
function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\homeDirs\"
        mkdir $baseHomeDir
       

    }

    process{
       
        $homefolder = $baseHomeDir+$_.Name

        mkdir $homefolder
        
       
        
    $ACL = Get-ACL -Path "C:\HomeDirs\$($_.Name)"
    $Name = "OU=" +$_.Name+ "," +"OU="+ $_.Klasse "," + $baseDn 
    $User = get-AdUser $Name
    
    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\HomeDirs\$($_.Name)" -AclObject $ACL



    }

    end{


    }



}

    		    

function MakeClean
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\homeDirs\ -Recurse     
   
}



Function moveGroups{
Move-ADObject -Identity OU=IT,DC=Win2016PS09,DC=local -TargetPath OU=GrpSchueler,OU=Schueler,DC=Win2016PS09,DC=localMove-ADObject 
Move-ADObject -Identity OU=MFA,DC=Win2016PS09,DC=local -TargetPath OU=GrpSchueler,OU=Schueler,DC=Win2016PS09,DC=localMove-ADObject
Move-ADObject -Identity OU=EH,DC=Win2016PS09,DC=local -TargetPath OU=GrpSchueler,OU=Schueler,DC=Win2016PS09,DC=localMove-ADObject 

Move-ADObject -Identity OU=18-FI1,DC=Win2016PS09,DC=local -TargetPath OU=IT,OU=GrpSchueler,OU=Schueler,DC=Win2016PS09,DC=localMove-ADObject 
Move-ADObject -Identity OU=18-MF3,DC=Win2016PS09,DC=local -TargetPath OU=MFA,OU=GrpSchueler,OU=Schueler,DC=Win2016PS09,DC=localMove-ADObject
Move-ADObject -Identity OU=18-EH2,DC=Win2016PS09,DC=local -TargetPath OU=EH,OU=GrpSchueler,OU=Schueler,DC=Win2016PS09,DC=localMove-ADObject 
}





$csvresult | setous 
$csvresult | setgroups
$csvresult | setuser 
$csvresult | set-HomeDir
$csvresult | setusertoGroups
#$csvresult | MakeClean
movegroups

