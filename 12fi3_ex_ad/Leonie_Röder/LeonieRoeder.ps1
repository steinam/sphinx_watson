﻿
$import = Import-Csv "C:\Temp\schule.csv" -Delimiter ","


$Domaininfo = get-AdDomain
$Domain = $domaininfo.DistinguishedName
$Group_List = @{}  


## Zum vereinfachen des Löschvorgangs
$Ueberordner = "OU=Schule,"+ $domain


function klassen
{
    begin{
    New-ADOrganizationalUnit -Name "Schule" -Path $Domain -ProtectedFromAccidentalDeletion $False
    }
    process{
    
    $OUKlasse = "OU="+$_.Klasse+","+$Ueberordner
    $name = $_.Klasse
    $name
    }
    end{
    New-ADOrganizationalUnit -Name $Name -Path $OUKlasse -ProtectedFromAccidentalDeletion $False
    }
}

function User
{
    begin{}
    process{
        Add-Member -InputObject $_ -Name Name -Value "" -MemberType NoteProperty
        $_.DName1 = "OU="+ $_.Gruppe1 +"," + $Ueberordner	
        $_.DName2 = "OU="+ $_.Gruppe2 +"," + $Ueberordner	
        $_.DName3 = "OU="+ $_.Gruppe3 +"," + $Ueberordner
        	
        

    	$HomeDir = "C:\HomeDirs\"+$_.beruf+"\"+$_.Klasse+"\"+$_.name

        $password = $_.Passwort | ConvertTo-SecureString -AsPlainText -Force
   
        New-ADUser -Path $_.DName1 -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $true -HomeDrive "H:" -HomeDirectory $HomeDir	    
        New-ADUser -Path $_.DName2 -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $true -HomeDrive "H:" -HomeDirectory $HomeDir	    
        New-ADUser -Path $_.DName3 -Enabled $True -SamAccountName $_.LoginName -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $true -HomeDrive "H:" -HomeDirectory $HomeDir	    

    }
    end{}
}


function grpSchueler
{
    begin
    { 
    New-ADOrganizationalUnit -Name "GrpSchueler" -Path $Ueberordner -ProtectedFromAccidentalDeletion $False 
    }
    process
    {
        $Group_List[$_.Gruppe1]=$_.Gruppe1
        $Group_List[$_.Gruppe2]=$_.Gruppe2
        $Group_List[$_.Gruppe3]=$_.Gruppe3
    }
    end{
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=Gruppen, " + $Ueberordner
	        New-ADGroup -Name $Group_List[$element] -Path $path -GroupScope Global
    }
}
}

function Homelaufwerke
{
    begin
    {
        $HomeDir = "C:\HomeDirs\"
        mkdir $HomeDir
    }
    process{
            $homefolder = $HomeDir+$_.beruf+"\"+$_.Klasse+"\"+$_.name
            mkdir $homefolder
        
       
        
    $ACL = Get-ACL -Path "C:\HomeDirs\$($_.beruf+"\"+$_.Klasse+"\"+$_.name)"
    $User = get-AdUser $_.name

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\HomeDirs\$($_.beruf+$_.Klasse+$_.name)" -AclObject $ACL

    }
    end{}
}


function UserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()
        $myGroups += $_.Gruppe1
        if($_.Gruppe2 -ne "")
        {
            $myGroups += $_.Gruppe2
        }
        if($_.Gruppe3 -ne "")
        {
             $myGroups += $_.Gruppe3
        }
        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.name 
        }
    }

    end{
    }
}

function removeALL
{
     Remove-ADOrganizationalUnit -Identity $Ueberordner -Confirm:$False -recursive:$True    
     rmdir C:\HomeDirs -Recurse  
}

########

removeAll

$import | klassen
$import | User
$import | grpSchueler
$import | Homelaufwerke
$import | UserToGroups
