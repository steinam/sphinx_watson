#Stegreifaufgabe_2_Jochen_Sauer

#User anlegen und Gruppen zuweisen

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



$baseDn = "OU=sauer.local,"+ $domainRoot

#Key = OUName, Value = DC=njd
$OU_HList = @{} #Aufnahme der Haupt-OUs
$OU_UList = @{} # Aufnahme der Unter-OUs
$Group_List= @{}




## csv-Datei einlesen
$csvresult = import-csv C:\temp\schule.csv -Delimiter ","

write-Host $csvresult



## diverse Funktionen
function setOUs{

    #Vor Beginn der Pipe
    begin{
    Write-Host "lege OUs an"
    New-ADOrganizationalUnit -Name "Schueler" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
    #hierbei wusste ich nicht, wie ich die Klassen in der OU anlegen sollte
    New-ADOrganizationalUnit -Name "Klassen" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
    }

    #F�r jede Zeile in der Pipe
    process{
        
         #Objekte der Pipeline erhalten eine neue Eigenschaft
         #DistinguishedName 
     

        $Ou_Name = $_.HauptOU
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU="+$_.HauptOU+"," + $baseDn
            $Ou_Name = $_.UnterOU

            $OU_UList[$OU_Name]= $OU_DN_Path
 
        }


    }


    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs hoffentlich angelegt"    
    }
}





function setGroups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Globale_Gruppe1]=$_.Globale_Gruppe1

        if($_.Globale_Gruppe2 -ne "")
        {
            $Group_List[$_.Globale_Gruppe2]=$_.Globale_Gruppe2
        }

        if($_.Globale_Gruppe3 -ne "")
        {
            $Group_List[$_.Globale_Gruppe3]=$_.Globale_Gruppe3
        }
    }

    end{   
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=GrpSchueler, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}


function  setUser
{
	begin{
	    Write-Debug "Adding User"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.HauptOU +"," + $baseDN	

        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU="+$_.HauptOU+"," + $baseDn
          #  $Ou_Name = $_.UnterOU

           


            $_.DName = "OU=" +$_.UnterOU +"," + $OU_DN_PATH    
        }



    	$HomeDir = "C:\home\"+$_.LoginName
        Write-Host $HomeDir

        #Passwortvergabe

        if("C:\home\"+$_.LoginName -eq "STE_2018"){
        $password = "EST_!!_7653" | ConvertTo-SecureString -AsPlainText -Force 
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword $password -HomeDrive "H:\" -HomeDirectory $HomeDir}
        
       ifelse("C:\home\"+$_.LoginName -eq "LAM_2018"){
        $password = "MAL_!!_8729" | ConvertTo-SecureString -AsPlainText -Force 
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword $password -HomeDrive "H:\" -HomeDirectory $HomeDir}	    

        elseif("C:\home\"+$_.LoginName -eq "MEI_2019"){
        $password = "MIE_!!_3846" | ConvertTo-SecureString -AsPlainText -Force 
   
        New-ADUser -Path $_.DName -Enabled $True -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword $password -HomeDrive "H:\" -HomeDirectory $HomeDir}
        
        else{
        Write-Host "error"}	   	    

        



}
	
	end{
	    write-debug "finished adding user"
	}
	
}



function setUserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Globale_Gruppe1
        if($_.Globale_Gruppe2 -ne "")
        {
            $myGroups += $_.Globale_Gruppe2
        }

        if($_.Globale_Gruppe3 -ne "")
        {
             $myGroups += $_.Globale_Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.LoginName
         }

    }

    end{
    }
}


function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\homeDirs\"
        mkdir $baseHomeDir

 #       mkdir "C:\homeDirs\IT\"
  #      mkdir "C:\homeDirs\EH\"
   #     mkdir "C:\homeDirs\MHA\"
        
#        mkdir "C:\homeDirs\IT\10FI1"
 #       mkdir "C:\homeDirs\IT\10EH2"
  #      mkdir "C:\homeDirs\IT\10MF31"
  #
    #    mkdir "C:\homeDirs\EH\10FI1"
   #     mkdir "C:\homeDirs\EH\10EH2"
     #   mkdir "C:\homeDirs\EH\10MF31"

      #  mkdir "C:\homeDirs\MHA\10FI1"
       # mkdir "C:\homeDirs\MHA\10EH2"
        #mkdir "C:\homeDirs\MHA\10MF31"


    }

    process{
       
        $homefolder = $baseHomeDir+$_.Beruf
        mkdir $homefolder

        $homefolder = $baseHomeDir+$_.Beruf+$_.klasse
        mkdir $homefolder
        
       
        
    $ACL = Get-ACL -Path "H:\$($_.Name)"
    $User = get-AdUser $_.LoginName

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"

    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [System.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [System.Security.accesscontrol.PropagationFlags]"None"
   


    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\HomeDirs\$($_.Anmeldename)" -AclObject $ACL


    }

    end{


    }



}



function remove-AllOUsAndGroups
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\home -Recurse   
     
     Remove-ADGroup  -Identity $baseDn -Confirm:$False -recursive:$True 
   
     Remove-ADUser -Identity $baseDn -Confirm:$False -recursive:$True 
}




#######Ablaufsteuerung########







#        Pipe    Funktion
remove-AllOUsAndGroups
$csvresult | setOUs
$csvresult | setGroups

$csvresult | setUser

$csvresult | setUserToGroups
$csvresult | set-HomeDir


##loschen der jeweiligen Teile des ADs
#Remove-ADUser
#Remove-ADGroup
#Remove-ADOrganizationalUnit
















