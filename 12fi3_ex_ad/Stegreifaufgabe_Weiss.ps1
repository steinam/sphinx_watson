﻿#Ausgeführt auf 10.204.0.6 - Passwort "Win-2017"

$domainInfos = Get-ADDomain;
$domainRoot = $domainInfos.DistinguishedName;
$baseDN = "OU=Schueler,$domainRoot";
$OU_KlasseList = @{};
$groupList = @{};

#OUs anlegen
function createOUs(){
    begin{
        Write-Host "OUs werden angelegt."
        try{
            New-ADOrganizationalUnit -Name "Schueler" -Path $domainRoot -ProtectedFromAccidentalDeletion:$false;
        } catch{
            Write-Host "OU Schueler existiert bereits -> Erzeugung überspringen"
        }
    }
    process{
        $OU_Name = $_.Klasse;
        $OU_DNPath = "" + $baseDN;

        $OU_KlasseList[$OU_Name] = $OU_DNPath;
    }
    end{
        foreach($element in $OU_KlasseList.Keys){
            New-ADOrganizationalUnit -Name $element -Path $OU_KlasseList[$element] -ProtectedFromAccidentalDeletion:$false;
        }

        Write-Host "OUs wurden erfolgreich angelegt."
    }
}

#User anlegen
function createUsers(){
    begin{
        Write-Host "User werden angelegt.";
    }
    process{
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty;
        $_.DName = "OU="+ $_.Klasse +"," + $baseDN;

        New-ADUser -Enabled $true -Path $_.DName -SamAccountName $_.LoginName -Name $_.Name -EmailAddress $_.Email -AccountPassword (ConvertTo-SecureString -force -AsPlainText $_.Passwort) -CannotChangePassword:$true;
    }
    end{
        Write-Host "User wurden erfolgreich angelegt."
    }
}

#Gruppen anlegen
function createGroups(){
    begin{
        Write-Host "Globale Gruppen werden angelegt."
        try{
            New-ADOrganizationalUnit -Name "GrpSchueler" -Path $baseDN -ProtectedFromAccidentalDeletion:$false;
        } catch{
            Write-Host "OU GrpSchueler existiert bereits -> Erzeugung überspringen"
        }
    }
    process{
        $groupName = $_.Gruppe1;
        $groupPath = "OU=GrpSchueler,$baseDN";

        $groupList[$groupName] = $groupPath;

        if($_.Gruppe2 -ne ""){
            $groupName = $_.Gruppe2;
            $groupPath = "OU=GrpSchueler,$baseDN";

            $groupList[$groupName] = $groupPath;
        }
        if($_.Globale_Gruppe3 -ne ""){
            $groupName = $_.Gruppe3;
            $groupPath = "OU=GrpSchueler,$baseDN";

            $groupList[$groupName] = $groupPath;
        }
    }
    end{
        foreach($group in $groupList.Keys){
            New-ADGroup -Name $group -Path $groupList[$group] -GroupScope Global;
        }
        Write-Host "Globale Gruppen wurden erfolgreich angelegt."
    }
}

#User den Gruppen zuordnen
function addUsersToGroups(){
    begin{
        Write-Host "Weise User den Gruppen zu.";
    }
    process{
        $myGroups = @();

        $myGroups += $_.Gruppe1;

        if($_.Gruppe2 -ne ""){
            $myGroups += $_.Gruppe2;
        }

        if($_.Gruppe3 -ne ""){
            $myGroups += $_.Gruppe3;
        }

        foreach($gruppe in $myGroups){
            Add-ADGroupMember $gruppe $_.LoginName; 
        }
    }
    end{
        Write-Host "User wurden erfolgreich den Gruppen zugewiesen."
    }
}

#HomeLaufwerke anlegen und zuordnen
function createHome(){
    begin{
        Write-Host "Home-Verzeichnisse werden angelegt.";
        $homebase = "C:\HomeDirs";
        try{
            mkdir $homebase;
        } catch{
            Write-Host "Die Homebase ist bereits angelegt.";
        }
    }
    process{
        $homedir = "$homebase\$($_.Beruf)\$($_.Klasse)\$($_.LoginName)";
        try{
            mkdir $homedir;
        }catch{
            Write-Host "Home-Verzeichnis für User $($_.LoginName) ist bereits angelegt."
        }

        $ACL = Get-Acl -Path $homedir;
        $user = Get-ADUser $_.LoginName;

        $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl";
        $inheritance = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit, ObjectInherit";
        $propagation = [System.Security.AccessControl.PropagationFlags]"None";
        $ruleDirection = [System.Security.AccessControl.AccessControlType]"Allow";

        $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($user.SID, $fileSystemRights, $inheritance, $propagation, $ruleDirection);

        $ACL.AddAccessRule($accessRule);

        Set-ACL -Path $homedir -AclObject $ACL;

        Set-ADUser -Identity $user -HomeDirectory $homedir -HomeDrive "H:";
    }
    end{
        Write-Host "Home-Verzeichnisse wurden erfolgreich angelegt.";
    }
}

#Cleanup
function MakeClean(){
    Write-Host "Cleanup brought to you by the `"Viscera Cleanup Detail`".";
    try{
        Remove-ADOrganizationalUnit -Identity $baseDN -Confirm:$False -Recursive:$True;
    }
    catch{
        Write-Host "Vorheriges Löschen der OUs nicht notwendig.";
    }
    try{
        rmdir "C:\HomeDirs" -Recurse;
    }catch{
        Write-Host "Home-Verzeichnisse nicht vorhanden.";
    }
    Write-Host "All clean and fresh! See you next time.";
}


################

#CSV einlesen
$csvresult = Import-Csv C:\Users\Administrator.WIN2016_AD_POWE\Desktop\schule.csv -Delimiter ",";

MakeClean;

$csvresult | createOUs;

$csvresult | createUsers;

$csvresult | createGroups;

$csvresult | addUsersToGroups;

$csvresult | createHome;