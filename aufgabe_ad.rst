=========
Aufgabe
=========

Sie planen ein Verwaltungstool für die UserAnlage innerhalb ihres ActiveDirectory. Die bisherige Vorgehensweise ist wie folgt:

- Es wird eine csv-datei produziert, die von weiteren Poweshell-Skripten abgearbeitet wird. Aus dieser csv-Datei werden dann die entsprechenden Container im AD erzeugt

  .. image:: figure/csv_ad.jpg
  
- Das Editieren der csv-Datei war bisher recht aufwändig und fehleranfällig. Sie planen deshalb, die Inhalte der csv-Datei in einer Datenbank zu speichern und Änderungen/Anpassungen der Werte mit Hilfe einer GUI nur innerhalb der Datenbank vorzunehmen. 

- Zuletzt wird aus den Datensätzen der Tabelle wiederum eine csv-Datei erzeugt.

- Die Datenbank mit der Tabelle *data* ist bereits vorhanden.

- Der Systembetreuer hat mit Ihnen bereits ein Konzept einer GUI erarbeitet.

  .. image:: figure/gui_ad.jpg
  
  Folgende Idee steckt hinter dieser GUI.

  .. image:: figure/gui_ad_function.jpg

  

.. sidebar:: Datei

	:download:`GUI <figure/gui_activedirectory.pff>`
		
  
.. admonition::  Aufgabe


	Eine GUI-Beschreibung mit Hilfe von PrimalForms liegt vor, wenngleich aber noch nicht alle Ereignishandler definiert wurden. Benutzen sie diese Datei als Ausgangspunkt für ihr Programm. 