﻿#Generated Form Function
function GenerateForm {

#region Import the Assemblies
[reflection.assembly]::loadwithpartialname("System.Windows.Forms") | Out-Null
[reflection.assembly]::loadwithpartialname("System.Drawing") | Out-Null
#endregion

#region Generated Form Objects
$form1 = New-Object System.Windows.Forms.Form
$setReports = New-Object System.Windows.Forms.Button
$getReports = New-Object System.Windows.Forms.Button
$dataGrid1 = New-Object System.Windows.Forms.DataGrid
$InitialFormWindowState = New-Object System.Windows.Forms.FormWindowState
#endregion Generated Form Objects

#----------------------------------------------
#Generated Event Script Blocks
#----------------------------------------------
#Provide Custom Code for events specified in PrimalForms.
$handler_getReport_Click= 
{
#TODO: Place custom script here

}

$handler_setReports_Click= 
{
#TODO: Place custom script here

}

$OnLoadForm_StateCorrection=
{#Correct the initial state of the form to prevent the .Net maximized form issue
	$form1.WindowState = $InitialFormWindowState
}

#----------------------------------------------
#region Generated Form Code
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 524
$System_Drawing_Size.Width = 847
$form1.ClientSize = $System_Drawing_Size
$form1.DataBindings.DefaultDataSourceUpdateMode = 0
$form1.Name = "form1"
$form1.Text = "Dr Watson Admin Tool"


$setReports.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 428
$System_Drawing_Point.Y = 475
$setReports.Location = $System_Drawing_Point
$setReports.Name = "setReports"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 46
$System_Drawing_Size.Width = 407
$setReports.Size = $System_Drawing_Size
$setReports.TabIndex = 2
$setReports.Text = "Änderungen an Datenbank übertragen"
$setReports.UseVisualStyleBackColor = $True
$setReports.add_Click($handler_setReports_Click)

$form1.Controls.Add($setReports)


$getReports.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 12
$System_Drawing_Point.Y = 475
$getReports.Location = $System_Drawing_Point
$getReports.Name = "getReports"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 46
$System_Drawing_Size.Width = 410
$getReports.Size = $System_Drawing_Size
$getReports.TabIndex = 1
$getReports.Text = "Daten von Datenbank abfragen"
$getReports.UseVisualStyleBackColor = $True
$getReports.add_Click($handler_getReport_Click)

$form1.Controls.Add($getReports)

$dataGrid1.DataBindings.DefaultDataSourceUpdateMode = 0
$dataGrid1.DataMember = ""
$dataGrid1.HeaderForeColor = [System.Drawing.Color]::FromArgb(255,0,0,0)
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 13
$System_Drawing_Point.Y = 12
$dataGrid1.Location = $System_Drawing_Point
$dataGrid1.Name = "dataGrid1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 465
$System_Drawing_Size.Width = 822
$dataGrid1.Size = $System_Drawing_Size
$dataGrid1.TabIndex = 0

$form1.Controls.Add($dataGrid1)

#endregion Generated Form Code

#Save the initial state of the form
$InitialFormWindowState = $form1.WindowState
#Init the OnLoad event to correct the initial state of the form
$form1.add_Load($OnLoadForm_StateCorrection)
#Show the Form
$form1.ShowDialog()| Out-Null

} #End Function

#Call the Function
GenerateForm
