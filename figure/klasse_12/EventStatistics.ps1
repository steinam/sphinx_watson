#Created by:Jeff Adkin

[void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")

#-- Form1 ---------------------------------------------------------------------
$Form1 = New-Object System.Windows.Forms.Form
$Form1.BackgroundImageLayout = [System.Windows.Forms.ImageLayout]::None
$Form1.ClientSize = New-Object System.Drawing.Size(1069, 809)
$Form1.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::Fixed3D
$Form1.Text = "Event Statistiken"
#region$Form1.BackgroundImage = ([System.Drawing.Image](...)
$Form1.BackgroundImage = ([System.Drawing.Image]([System.Drawing.Image]::FromStream((New-Object System.IO.MemoryStream(($$ = [System.Convert]::FromBase64String(
"/9j/4AAQSkZJRgABAQEAYABgAAD/4QBoRXhpZgAATU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUA"+
                                 "AAABAAAARgEoAAMAAAABAAIAAAExAAIAAAASAAAATgAAAAAAAABgAAAAAQAAAGAAAAABUGFpbnQu"+
                                 "TkVUIHYzLjUuMTAA/9sAQwAQCwwODAoQDg0OEhEQExgoGhgWFhgxIyUdKDozPTw5Mzg3QEhcTkBE"+
                                 "V0U3OFBtUVdfYmdoZz5NcXlwZHhcZWdj/9sAQwEREhIYFRgvGhovY0I4QmNjY2NjY2NjY2NjY2Nj"+
                                 "Y2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2Nj/8AAEQgAdwQyAwEiAAIRAQMRAf/E"+
                                 "AB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAE"+
                                 "EQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZH"+
                                 "SElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1"+
                                 "tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEB"+
                                 "AQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXET"+
                                 "IjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFla"+
                                 "Y2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXG"+
                                 "x8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8AwLjxBq8dzIgv51CS"+
                                 "NgByccnjJ6iox4i1jj/iYz8En73+fWqN2u27mG1VxIw2r0HPQVFXSoohmn/wkWsDH/EwuOCT97/P"+
                                 "rR/wkWsDH/EwuOCT97/PrWZRVcq7E3NP/hItYGP+JhccEn73+fWl/wCEi1cY/wCJhccEn73+fWsv"+
                                 "FLTUV2C5p/8ACRavx/xMLjgk/e/z60f8JDrAx/xMZ+CT97/PrWZRT5V2E2zTHiHV+P8AiYz8En73"+
                                 "+fWl/wCEh1cY/wCJjPwSfvf59azKKajHsF2aY8Q6vx/xMLjgk/e/z60o8Qavx/xMJ+CT97/PrWZR"+
                                 "T5I9hXZpjxBq/H/Ewn4JP3v8+tH/AAkGrjH/ABMJ+CT97/PrWbS4quSPYLs0h4g1fj/iYT8En73+"+
                                 "fWlGv6tx/wATCfgk/e/z61mgU4Cq5I9iXJmiNf1bj/iYT8En73+fWnDXtV4/4mE/Bz9+s4LSgVXJ"+
                                 "HsS5PuaI13VeP9Pn4Ofv0o13VeP9Pn4Ofv1n4pwFV7OHYnmfc0Brmqcf6fPwc/fpw1vVOP8ATp+D"+
                                 "n75qgBTwKfs4diHOXcvDWtT4/wBOn4Ofv0o1nU+P9On4OfvmqQFPAFUqcOyIdSXctjWNT4/06fg5"+
                                 "++acNX1Lj/TZ+Dn75qoAKUCrVKHZEOpLuXBq2o8f6bP1z980o1bUf+f2frn75qoBTgtP2MP5US6s"+
                                 "u5aGrajx/pk/XP3zSjVdQ/5/J+ufvmqwWnBar2MP5V9xLqz7lkapqH/P5P1z980o1PUP+fyfrn75"+
                                 "quFpwSq9jT/lX3C9rPuycanf/wDP5P8A99mlGpX/APz+T/8AfZqEIKUIKPY0/wCVfcT7Wfdkw1G/"+
                                 "/wCfyf8A77NKNRv/APn8n/77NRBBTtlL2dP+VfcHtp92SjUL7/n7m/77NH9oX3/P3N/32ajCUuyj"+
                                 "2dP+VfcHtp92SC/vv+fub/vs0v2+9/5+5v8Avs1HspdlL2dP+VfcL20+7H/b77/n7m/77NKL+9/5"+
                                 "+5v++zTAlLspclP+VfcHtZ/zMd9uvf8An7m/77NH269/5+5v++zTdlGyjkp/yr7g9rP+Z/eO+3Xv"+
                                 "/P3P/wB9ml+3Xv8Az9Tf99mm7PagJS5Kf8q+4Paz/mY77de/8/U3/fZo+3Xv/P1N/wB9mk2UbKOS"+
                                 "n/KvuD2s/wCZi/br3/n6m/77NL9tvf8An6m/77NN8ujZS5Kf8q+4Paz/AJn9477be/8AP1N/32aP"+
                                 "tt7/AM/c3/fZpNlHl0clP+VfcHtZ/wAz+8X7be/8/U3/AH2aPtt7/wA/U3/fZpAlLsFHJT/lX3B7"+
                                 "Wf8AM/vD7be/8/c3/fZo+23v/P3N/wB9mjy/ajy6OSn/ACr7he2n/Mw+23v/AD9zf99mj7be/wDP"+
                                 "3N/32aPLo8ujkp/yr7h+2n/Mw+23v/P3N/32aPtt7/z9zf8AfZo8ul8ujkp/yr7g9rP+Z/eJ9tvf"+
                                 "+fub/vs0v2y9/wCfub/vs0eXR5dHJT/lX3B7af8AMw+2Xv8Az9zf99mj7Ze/8/c3/fZo8ujy6XJT"+
                                 "/lX3C9tP+Zh9svf+fqb/AL7NH2y9/wCfqb/vs0vl0bKOSn/KvuD20/5mH2y9/wCfub/vs0C8vP8A"+
                                 "n6m/77NGygJRyU/5V9we2n/Mw+2Xn/P1N/32aPtl5/z9Tf8AfZpdlJ5dHJT/AJV9we2n/Mw+2Xn/"+
                                 "AD9Tf99ml+13n/P1N/32aNgpdlHJT/lX3B7af8zE+13n/P1N/wB9mj7Xef8AP1N/32aXZRs9qOSn"+
                                 "/KvuD20+7E+13n/P1N/32aPtl5/z9Tf9/DRso20clP8AlX3B7af8zF+13n/PzN/38NH2u8/5+Zv+"+
                                 "/ho20bKXJT/lX3B7afdii7u/+fmb/v4aPtd3/wA/M3/fw0bKNlHLD+VB7afdh9qu/wDn6m/77NH2"+
                                 "q7/5+pv++zRto2UuWH8qD20+7D7Vd/8AP1N/32aPtV3/AM/U3/fZo2UbaOWH8qD20+7F+1Xf/P1N"+
                                 "/wB9mj7Vd/8AP1N/32aTbRso5YfyoPbT7sX7Vd/8/U3/AH2aT7Vd/wDP1N/32aXZSbKOWH8qD20/"+
                                 "5mH2u7/5+pv++zR9ru/+fqb/AL7NJso2U+SH8q+4ftZ/zP7xftd3/wA/U3/fZo+13f8Az8zf9/DS"+
                                 "baNlHJD+VB7Wf8zF+2Xf/PzN/wB/DR9ru/8An6m/77NN2e1Gz2o5IdkHtp92L9ru/wDn6m/77NH2"+
                                 "y7/5+pv++zSbfajb7UckP5UHtZ/zP7xftl3/AM/U3/fZo+2Xf/P1N/32aQpSbKfJT/lX3B7Wf8z+"+
                                 "8X7Zef8AP1N/32aPtl5/z9Tf99mk20baOSn/ACr7g9tPuxftl5/z9Tf99mj7Zef8/U3/AH2aTbRt"+
                                 "o5Kf8q+4PbT7sX7Zef8AP1N/32aPtl5/z9Tf99mk20myjkp/yr7g9tPux32y8/5+pv8Avs0n2y8/"+
                                 "5+pv++zSbKNtHJD+VfcHtp92L9svP+fqb/vs0fbbz/n6m/77NIVpNtHJD+VfcHtp92O+23n/AD9T"+
                                 "f99mj7bef8/U3/fZpu2jbT5Kf8q+4PbT7sd9svP+fqb/AL7NH228/wCfqb/vs03bRso5Kf8AKvuD"+
                                 "20+7F+23n/P1N/32aX7bef8AP1N/38NM2UbaOSH8q+4Paz7sf9tvP+fqb/v4aT7bef8AP1N/32ab"+
                                 "to20clP+VfcHtZ/zMd9tvP8An6m/77NH228/5+pv++zTSlJs9qOSn/KvuD2s/wCZj/tt5/z9Tf8A"+
                                 "fZo+23n/AD9Tf99mmbaNtP2dP+VfcHtZ92O+23v/AD9Tf99mj7de/wDP1N/32abto2Uezp/yr7g9"+
                                 "rPux3269/wCfqb/vs0fbrz/n6m/77NM2UbKPZ0/5V9we1n/Mx/229/5+5v8Avs0n269/5+5v++zT"+
                                 "dtG32o5Kf8q+4ftZ92O+3Xv/AD9zf99mj7de/wDP3N/32aZt9qNtHJT/AJV9we1qd2P+3Xv/AD9T"+
                                 "f99mj7de/wDP1N/38NM2UbKfs6f8q+4XtZ/zMd9uvv8An6m/7+Gj7dff8/U3/fZpm2jbR7On/Kvu"+
                                 "H7Wf8zH/AG69/wCfub/vs0fb73/n7m/77NM20hWj2dP+VfcHtZ/zMf8Abb3/AJ+5/wDv4aKZtoqu"+
                                 "WPYXPLuchdjbdzDaq4kYbV6DnoKiqa7XbdzDaq4dhtXoOegqKvJR7TDFFFLVCEopaQVSQhcUuKO1"+
                                 "Ap2EGKMUtKKpIBMUuKMUuKpIlsQCnAUlPFVYm4gFKBSilAqkiWxQKcBQBThVJENgBTgKBTxVJEtg"+
                                 "AKcAKAKeBVWIbACnAUACnCqSM2wApwFKBTgK0SIbEC08ClApwFUkQ2Iq08LSgU8CgkQLTgtKBTwt"+
                                 "S2A0LShakC0u2p5gGBacFpwFOApNiGhaXbTwKUCobAZto21JilAouIYFxRtqTFLilcCLbS7akxSg"+
                                 "UrgR7BQFqXFAWi4Ee2jbUm2lC0rhci20u2pMUu2i4rkW2jbUu2jFFwuR7aNtShaNtK4XIttG2pcU"+
                                 "baLhcj20bak20baLhcj20bakxS7aLhcj20bKk20YouFyPbRtqXFGKVwuRbaNtS4oxRcLke2jbUuK"+
                                 "MUXC5Ft9qNtS4oxRcLkW2jbUuKMUXC5Ftpdv0p+KNtFwuR7aNtSYpcUXC5Ft+lKFqTFGKLhcZtpN"+
                                 "tSYoxSuFyPbRtqTFFFwuR7aNtSUYouFyMrRin4pcCi4XI8UmKlxSYouFyPFJtqXFJincLke2jbUm"+
                                 "KTFFwuM2+1JipNtGKLjI8UYqTFGKLgR4oxT8UmKdwGbaNtSYoxRcCPbRtqTFIRRcBmKMe1PxRii4"+
                                 "DMUmKkxRgUXAjxRin4oxTuAzFGKfijFFwI8UYp+KMUXAZikxUlGKdxkeKMVJikxRcQzFGKfijFFw"+
                                 "GbaNtPxRincZHijFPxRii4DMUm2pMUYouBFilxT9tGKLgRlaNtSYoxRcRHto21JijAp3GRbaNtSY"+
                                 "oxRcCPFBFSYpCKLjI8UVJiii4HEXY23cw2quHYbV6DnoKiqa6XbdzDaq4dhtXoOegqKvNWx7rEox"+
                                 "S0oqyQFFFLVITExSiijFOwhaMUUoqkgClxSU4VRLYCnAUgFOAqiGwApwFApwpolsAKeKQU4VRDYo"+
                                 "FOApop4FUiWOFOFIKcKtGbFFOFIKdVIhjhTgKaKeKtEMcBTwKaKeKZLHAU8U0U4UmIcBTwKaKeKh"+
                                 "iFFPFNFOBqWA4ClxSClFSxDgKUUgpakBRS0lGaQDqKSlFIQuKWkzS0AGKMUA04UgEApcUUUgYYpa"+
                                 "M0tIQmKMUtFAABRiilzQAlFLmigBKMUtFIBMUuKKXNACYopc0UAJijFLRQAmKMUtFABijFLmigBM"+
                                 "UYpaKAExRilzRQAhFJinUlACYoxS0UAJRS0UAFFFFABRRRQAUUUUAIaKWkoAKKKKAExQRRRTATFG"+
                                 "KWigBKMUuaSmFwxSYpaKBiYox9aWjNADaKWigBKMUtIaYBiiiigApKWigBKKWkoGJRS0UCEopaDQ"+
                                 "AlJS0UxiUc0tFACYoxS0ZoASiiigBMUYpaKAExRilooATFJTs0lACYoxS0UwExRig0UAJijFLRRc"+
                                 "BKQiloNMYmKKKKYHD3Q23cw2quHYbV6DnoKiFS3Q23cw2quHYbV6DnoKiFeetj3RcUUUtWhMSloo"+
                                 "FUhAKWilpgAFLRRVIhgBTgKQU6qQmAFOpBThVEMBTxTR9KcKpEsUU8U0U4UyGOHNOFNFKKpEseKc"+
                                 "KaDThVozY4U6mCn1RLHCng1GKeDVIljwaeKiBp4NMlkoNOBqMGnA0CJM04Go80oNS0ImDU4GoQac"+
                                 "GqWhEoNOBqIGl3VDQE2aUGoQ1PDVNgJAaXNRbqXdRYRLmlBqIGlzSsBJnmlBqMGlz70rASZoDVHn"+
                                 "3o3e9FhEuaUGog1KDSaAkzRmmZozSsBIDS5qLNLmiwEmaM0wGjNFgH5ozTMijIpWAkzRmmbqN1Fg"+
                                 "H5ozTM0Z96LCH5ozTM+9GaLAPzS5pgNGaLAPzRmmZozRYB+aM0zNGaVgH596M+9MzRmiwD8+9FMz"+
                                 "RmiwD80Zpm6jdRYB+aM0zdRmiwx+aM0zNGadgH5ozTN1GeKVgH5pM03PFGaLAOzS5pmaM07AOzmi"+
                                 "m5oz70WAdmkzTc0ZosA7NGabRmgB2aCabmkzRYB2aM03NGadgHZozTc0maLAPJpM03NFFgHZozTM"+
                                 "0ZosMfn3oz70zNGaLAOz70Zpu6jNMB2aM03NGaAHE0lNJozQA7NGabmjNFgH5pCabmgmiwDs0mab"+
                                 "kUZp2AdmjNNzRmiwDs0maaTRmiwDs0ZpuaM0WAdmjNN3UZosA7NGabmkJp2AdmjNNzSZosA/NGaZ"+
                                 "mjNFgH5pM03NGaLAOzRTd1G6iwC/jQcetNzRmnYYuRRTc0UWA4q6G27mG1Vw7DavQc9BUQqW6G27"+
                                 "mG1Vw7DavQc9BUVeetj3mLSikpaskKWkpapCClFJS1QgpRQaBVIliinU0U6miWKKcKaKcKoliinC"+
                                 "minCqRDHCnCm0opiHU4U3rSimiR4NOBFMBpQapMhofTgaYDTgau5LHg0oNMzSg00SyQGnA1GKcDV"+
                                 "XJJAacDUQNOBpk2JQacDUINOBpiJg1KGqIMaUNUgTBjS7qiBpd1KwiYGlDVCGpd1KwE26lDVFuoB"+
                                 "pWCxNupQ1Q5pQanlETbqNwqLdS5osBLuozUe6gNmlYCUNS7qizRupWAmDUu6oQ1LmlYRLupc1Dup"+
                                 "d1FgJc0bqiDUu6iwEm40ZNR7qN1KwEuaM1FmjNFgJc0ZqPdRuosBJmjNR7qN1FgJM0uTUQal3UWA"+
                                 "k3Ubqj3UZpWAk3GjdUeaM0WAk3UbqjzRmiwEm40uaizS7qLASZo3VHupM0WAl3CjcKizRmiwEm6j"+
                                 "dUeaM0WAk3UbqjzRmiwEm40bjUeaM0WAk3GjcajzRmiwEm6jdUeaM0WAk3UbqZmkzRYCTdRuqLNG"+
                                 "adgJN1G6o80ZosBJupN1MzRmiwD91G6o91G6iwEm6jdUe6jdTsA/dRuqPNGaLASbjQTUeaM0WGP3"+
                                 "UbqZmjNFgH7qNxpmaTNFgJM0ZqPNG6iwEmaM1HmkzTsBLmjNRZNGaLAS5pM1HmjdRYCTNGaj3Ubq"+
                                 "LASZpN1R7qM0WAk3UbqjzRmiwEmaTNMzQTTsA/dRuqPNGaVgJN1GajzRup2AfmjdTN1GadgH5pM0"+
                                 "zdijdRYB+aM1HmjNFgH7qN1R5ozTsMfuopmaKLAcjdDbdzDaq4dhtXoOegqKpbobbuYbVXDsNq9B"+
                                 "z0FRV5a2PdYoo/GkpRVEh+NOptAq0IdmlFJSimAv1pQB6ikoqkSx1FNp1USxRThTRS1RLHilFMFO"+
                                 "FNEsdThTRSg1RNhwpwpuaUGmS0PGKXimZpQaZNiQUtMBp2aomw4GnZpgNLmmiWh4NLmmA0tUS0PB"+
                                 "pQaZmlBpoViQGnA1GDSg07k2JAaUGos07NO4WJQaXNRA0uaBWJgaM1EDS5oESg0oNRg0uaAJN1KG"+
                                 "qLNKDSsFiXdS7qhyaUE0WCxLupQaizS5qbATZozUQJ9aXNFhWJM07NQ5pc0rBYk3UoaogaXNKwWJ"+
                                 "M0uaizRuosBJmlBqPI9KM0WCxLmjNRZozRYLEuaM1FmlzSsBJmjdUeaM0WESBqXNRA0uaLASbqN1"+
                                 "R7qN1KwyTdRupmaM0WAkzRmo80ZosBJmjNR5ozRYCTNGajzRmiwEmaM1HmjNFgJM0ZqPNGaLASZo"+
                                 "zUeaM0WAkzRmo8/WjdRYCTNG6o91G6iwEm6jNR7qM0WEPzRmo80ZosBJmjNR5pCaLDJc0m6o80Zp"+
                                 "2ESbqN1R5ozRYB+aMimZozRYdh+aM0zNGfpRYLD80ZqPNGaLBYkzRmo80ZosFiTNGajzRmiwWH5o"+
                                 "zTM0Zp2Cw/NG6oyaM0WGSZozUW6jdRYCXdSbqZmjNFgH5pM03NJup2AfmjcKZuNGaLAPzRmmZoz9"+
                                 "aLAPzRmoyaMmiwEmfekJ96ZmjNFgH5ozTM0hNOwD91GaZmjNFgHZozTc0maLAPzRmmZozRYB+aTN"+
                                 "MzRmnYB2aM0zNGadgH5opmaKLAcxdDbdzDaq4dhtXoOegqKpbobbuYbVXDsNq9Bz0FRV4yPdYUop"+
                                 "KUVRIUtJS1SEFKKSlFUIXNLSUVQh1GaTNKKZLQoNOzTaM1RLQ8GlBpoNKKpEsdmlBNNBpQadyR4N"+
                                 "OFR5p2aolju9KKZThQFh4NKDTAacKdybDxS0zNKDTTJaH5pc0zNKKq5Nh+aUGmCjNUmKxJmlDUzO"+
                                 "aM0XESZpc1GDSg00BIDS5qPNKDVXJJAaXNMBozRcTJN1KGqPNGaLgSZpQaYDS5ouFh+6lDVHmlzR"+
                                 "cLEm6jdUeaWgmxJmlzUeaM0wsSZpd1R5ozQBIDS5qPNGaQrEmaN1R5ozQFiXdRuqPNGaBkm6jNR5"+
                                 "pQaAH5PrRmmZpc+9FhWH7qN1R5ozSsBIGpd1R5ozRYCTNGajzS5osA/dRupmaAaLASbqN1R5ozSs"+
                                 "BJmjNMzRmiwD80ZpmaM0WAfmjdTM0maLBYkzRmmZozRYLD8mjNMzRmiwD80bqZmjNFgH7qN1MzSZ"+
                                 "osBJmjd71HmjNFgH5ozTM0ZosA/NGaZmjNFgsP3UmaZmjNOwx+aM0zNGaLAPzRmmZozRYB+aM0wm"+
                                 "kzQBJuozUeaM07APzRmmZozRYB+aM0zNGaLAPzRmmZozRYB+aN1MzSZoAkzRmo80ZoAfmjNMzRmg"+
                                 "B+fejNM3UmaAsSZpM0zNGaAsPzRmmZozQFh+aM0zNJmgCTdRmo80ZoAfmjNMzRmgLD80ZpmaM0ws"+
                                 "PzSZpmaM0BYfmjdTM0ZoCw7NGaZmjNAWHZozTc0hNBVh+6imZooCxzt2Nt3MNqrh2G1eg56CoqKK"+
                                 "8VbHtMKUUUVRIUtFFNAFKKKKpEi0UUUwAU4UUVSExaKKKokUUoooqkSxwpQaKKokXNKDRRQSxacK"+
                                 "KKZIopc0UUIABpaKKoljhS5oooJFB5pc0UVSExc0ZoopiFBp2aKKoABpQaKKCRc0ZoooAcDRmiim"+
                                 "IM0oNFFMQuaM0UUALmlBoooEGaM0UUwHA0ZoooAUGjNFFAgzRmiigBc0ZoooAM0oNFFABml3UUUA"+
                                 "GaM0UUAGaM0UUALmjNFFBIZozRRQUGaM0UUAGaXNFFABmjNFFABmjNFFABmjNFFAgzQTRRQAmaM0"+
                                 "UUAGaM0UUBYM0ZoooGGaM0UUAGaM0UUAGaTNFFABmjNFFABmjdRRQAZozRRQAZozRRQAZoHJwOpo"+
                                 "ooACSG2HqO9JmiigYZozRRQAZozRRQIM0ZoooGGaCaKKAEozRRQAZpMn1oooEGaM0UUDQZozRRQA"+
                                 "maM0UUAFGaKKADNJmiigAzRmiigAJpM0UUDDNGaKKADNGaKKAEzRRRQB/9k=")),0,$$.Length)))))

#-- Label14 -------------------------------------------------------------------
$Label14 = New-Object System.Windows.Forms.Label
$Label14.Font = New-Object System.Drawing.Font("Cooper Black", 18.0, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label14.Location = New-Object System.Drawing.Point(384, 73)
$Label14.Size = New-Object System.Drawing.Size(317, 29)
$Label14.TabIndex = 14
$Label14.Text = "Loading... Bitte warten..."
$Label14.BackColor = [System.Drawing.Color]::Transparent
$Label14.ForeColor = [System.Drawing.Color]::DarkRed
$Label14.visible = $false
#-- Label15 -------------------------------------------------------------------
$Label15 = New-Object System.Windows.Forms.Label
$Label15.Font = New-Object System.Drawing.Font("Cooper Black", 15.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label15.Location = New-Object System.Drawing.Point(61, 73)
$Label15.Size = New-Object System.Drawing.Size(409, 26)
$Label15.TabIndex = 15
$Label15.Text = "Statistiken für Event ID"
$Label15.BackColor = [System.Drawing.Color]::Transparent
$Label15.ForeColor = [System.Drawing.SystemColors]::ButtonFace
$Label15.Visible = $false
#-- Label13 -------------------------------------------------------------------
$Label13 = New-Object System.Windows.Forms.Label
$Label13.Font = New-Object System.Drawing.Font("Cooper Black", 24.0, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label13.Location = New-Object System.Drawing.Point(759, 20)
$Label13.Size = New-Object System.Drawing.Size(282, 48)
$Label13.TabIndex = 13
$Label13.Text = "(BETA Version)"
$Label13.BackColor = [System.Drawing.Color]::Transparent
$Label13.ForeColor = [System.Drawing.SystemColors]::ButtonFace
#-- Label12 -------------------------------------------------------------------
$Label12 = New-Object System.Windows.Forms.Label
$Label12.Font = New-Object System.Drawing.Font("Cooper Black", 26.25, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label12.Location = New-Object System.Drawing.Point(15, 20)
$Label12.Size = New-Object System.Drawing.Size(599, 53)
$Label12.TabIndex = 12
$Label12.Text = "Event Statistics V.1"
$Label12.BackColor = [System.Drawing.Color]::Transparent
$Label12.ForeColor = [System.Drawing.SystemColors]::ButtonFace
#-- PictureBox1 ---------------------------------------------------------------
$PictureBox1 = New-Object System.Windows.Forms.PictureBox
$PictureBox1.BackgroundImageLayout = [System.Windows.Forms.ImageLayout]::Center
$PictureBox1.Location = New-Object System.Drawing.Point(1030, 631)
$PictureBox1.Size = New-Object System.Drawing.Size(23, 18)
$PictureBox1.SizeMode = [System.Windows.Forms.PictureBoxSizeMode]::StretchImage
$PictureBox1.TabIndex = 11
$PictureBox1.TabStop = $false
$PictureBox1.Text = ""
#region$PictureBox1.Image = ([System.Drawing.Image](...)
$PictureBox1.Image = ([System.Drawing.Image]([System.Drawing.Image]::FromStream((New-Object System.IO.MemoryStream(($$ = [System.Convert]::FromBase64String(
"/9j/4AAQSkZJRgABAQEASABIAAD/4QD6RXhpZgAATU0AKgAAAAgABgEaAAUAAAABAAAAVgEbAAUA"+
                                 "AAABAAAAXgEoAAMAAAABAAIAAAExAAIAAAASAAAAZgEyAAIAAAAUAAAAeIdpAAQAAAABAAAAjAAA"+
                                 "ALgAAABIAAAAAQAAAEgAAAABUGFpbnQuTkVUIHYzLjUuMTAAMjAwODowODoxOSAyMzowNjowNAAA"+
                                 "A6ABAAMAAAAB//8AAKACAAQAAAABAAABLKADAAQAAAABAAABLAAAAAAAAAADARoABQAAAAEAAADi"+
                                 "ARsABQAAAAEAAADqASgAAwAAAAEAAgAAAAAAAAAAAEgAAAABAAAASAAAAAH/2wBDABALDA4MChAO"+
                                 "DQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/"+
                                 "2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2Nj"+
                                 "Y2NjY2NjY2NjY2P/wAARCAAtAC0DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQF"+
                                 "BgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS"+
                                 "0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4"+
                                 "eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi"+
                                 "4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREA"+
                                 "AgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl"+
                                 "8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImK"+
                                 "kpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP0"+
                                 "9fb3+Pn6/9oADAMBAAIRAxEAPwDvpHWNC7nAFcxqPiJ2lMdsxUDsnX8T/QfnTvE2qHP2GA8k/vGH"+
                                 "XHpWdY2Ee4CQAnAZh2XPQfWqjFt2Qm0ldipd3cxyWP6n+dXbV7wn9zcOjfmPyq4ht0XaIEIHqM1F"+
                                 "Nbo37+2UwyIeGXp9CK09kzP2iNOzvZd4gvUCSkZVl+69aFcxcX7T26uSFlQ8j+6w/wA/ka2tLvlv"+
                                 "7USY2uvDr6GsmrGidzipHM0k9w3OSSa1dEh+2OYzIVJXeTjPOf8A69VorEtdX9g/BSRgP91uVP5G"+
                                 "qNpeXOnyNHuMU8eUf/P5VrS6pbmdTpc7DUII7PS32DnIyx6nmmQbPNSFMiK5g3bSc4NY1xr32vS3"+
                                 "t5xibIww6Nz+hp9vrEflzXcrqs6RiGGJRwPeq5ZW1I5o30KtviXWYIu0wbI+nI/matzXf9m6pcxw"+
                                 "Hh1Rj9cEf0rK0lzJqhuukdshUH1Zv/rVqaHaLq93f3c2fKDrHGfXaDn+YrOo/eZrTXuo1dXs5Uuo"+
                                 "9Us4/MmiG2WIdZU9vcdRWLqwttSC3cJAcjGccMB2PuPzH6V2VZt7o1vcyNNGzW8zcsyAEMf9pTwf"+
                                 "r196hOxTVzjE0m+l5it5G+hU/qSP5U4aLdD/AI+nECd+QW/IZH6/hWxfX97pTG33wSf7QiKn/wBC"+
                                 "NPs9Mm1ePzrm8KRnqkUeCfxJP8qv2ku5Ps49jLjieZl07TU+cjr1CDu7H/Oa7LTrGLTrGK1h+7GO"+
                                 "p6se5P1NLY2Ftp8PlWsQRTyT1LH1J6mrNZln/9k=")),0,$$.Length)))))
$PictureBox1.add_Click({PictureBox1Click($PictureBox1)})
#-- GroupBox3 -----------------------------------------------------------------
$GroupBox3 = New-Object System.Windows.Forms.GroupBox
$GroupBox3.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$GroupBox3.Location = New-Object System.Drawing.Point(12, 621)
$GroupBox3.Size = New-Object System.Drawing.Size(1047, 179)
$GroupBox3.TabIndex = 10
$GroupBox3.TabStop = $false
$GroupBox3.Text = "Messages"
#-- Label11 -------------------------------------------------------------------
$Label11 = New-Object System.Windows.Forms.Label
$Label11.Location = New-Object System.Drawing.Point(13, 108)
$Label11.Size = New-Object System.Drawing.Size(999, 57)
$Label11.TabIndex = 1
$Label11.Text = "Additional Information: "
#-- Label10 -------------------------------------------------------------------
$Label10 = New-Object System.Windows.Forms.Label
$Label10.Location = New-Object System.Drawing.Point(13, 28)
$Label10.Size = New-Object System.Drawing.Size(1005, 61)
$Label10.TabIndex = 0
$Label10.Text = "Message: "
$GroupBox3.Controls.Add($Label11)
$GroupBox3.Controls.Add($Label10)
#-- GroupBox2 -----------------------------------------------------------------
$GroupBox2 = New-Object System.Windows.Forms.GroupBox
$GroupBox2.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$GroupBox2.Location = New-Object System.Drawing.Point(12, 351)
$GroupBox2.Size = New-Object System.Drawing.Size(307, 254)
$GroupBox2.TabIndex = 9
$GroupBox2.TabStop = $false
$GroupBox2.Text = "Event"
#-- Label9 --------------------------------------------------------------------
$Label9 = New-Object System.Windows.Forms.Label
$Label9.Location = New-Object System.Drawing.Point(11, 203)
$Label9.Size = New-Object System.Drawing.Size(245, 31)
$Label9.TabIndex = 5
$Label9.Text = "User ID: "
#-- Label8 --------------------------------------------------------------------
$Label8 = New-Object System.Windows.Forms.Label
$Label8.Location = New-Object System.Drawing.Point(10, 167)
$Label8.Size = New-Object System.Drawing.Size(248, 24)
$Label8.TabIndex = 4
$Label8.Text = "Instance ID: "
#-- Label7 --------------------------------------------------------------------
$Label7 = New-Object System.Windows.Forms.Label
$Label7.Location = New-Object System.Drawing.Point(11, 130)
$Label7.Size = New-Object System.Drawing.Size(249, 24)
$Label7.TabIndex = 3
$Label7.Text = "Event ID: "
#-- Label6 --------------------------------------------------------------------
$Label6 = New-Object System.Windows.Forms.Label
$Label6.Location = New-Object System.Drawing.Point(10, 93)
$Label6.Size = New-Object System.Drawing.Size(249, 20)
$Label6.TabIndex = 2
$Label6.Text = "Source: "
#-- Label5 --------------------------------------------------------------------
$Label5 = New-Object System.Windows.Forms.Label
$Label5.Location = New-Object System.Drawing.Point(11, 58)
$Label5.Size = New-Object System.Drawing.Size(250, 26)
$Label5.TabIndex = 1
$Label5.Text = "Entry type: "
#-- Label1 --------------------------------------------------------------------
$Label1 = New-Object System.Windows.Forms.Label
$Label1.Location = New-Object System.Drawing.Point(11, 24)
$Label1.Size = New-Object System.Drawing.Size(286, 25)
$Label1.TabIndex = 0
$Label1.Text = "Last Generated: "
$GroupBox2.Controls.Add($Label9)
$GroupBox2.Controls.Add($Label8)
$GroupBox2.Controls.Add($Label7)
$GroupBox2.Controls.Add($Label6)
$GroupBox2.Controls.Add($Label5)
$GroupBox2.Controls.Add($Label1)
#-- GroupBox1 -----------------------------------------------------------------
$GroupBox1 = New-Object System.Windows.Forms.GroupBox
$GroupBox1.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Bold, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$GroupBox1.Location = New-Object System.Drawing.Point(12, 134)
$GroupBox1.Size = New-Object System.Drawing.Size(307, 197)
$GroupBox1.TabIndex = 8
$GroupBox1.TabStop = $false
$GroupBox1.Text = "Input"
#-- Button1 -------------------------------------------------------------------
$Button1 = New-Object System.Windows.Forms.Button
$Button1.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Button1.Location = New-Object System.Drawing.Point(70, 162)
$Button1.Size = New-Object System.Drawing.Size(154, 24)
$Button1.TabIndex = 8
$Button1.Text = "Continue"
$Button1.UseVisualStyleBackColor = $true
$Button1.add_Click({Button1Click($Button1)})
#-- Label2 --------------------------------------------------------------------
$Label2 = New-Object System.Windows.Forms.Label
$Label2.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label2.Location = New-Object System.Drawing.Point(10, 29)
$Label2.Size = New-Object System.Drawing.Size(59, 21)
$Label2.TabIndex = 5
$Label2.Text = "Event ID"
#-- Label4 --------------------------------------------------------------------
$Label4 = New-Object System.Windows.Forms.Label
$Label4.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label4.Location = New-Object System.Drawing.Point(10, 124)
$Label4.Size = New-Object System.Drawing.Size(58, 15)
$Label4.TabIndex = 7
$Label4.Text = "Zeitraum"
#-- ListBox1 ------------------------------------------------------------------
$ListBox1 = New-Object System.Windows.Forms.ComboBox
$ListBox1.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$ListBox1.FormattingEnabled = $true
$ListBox1.ItemHeight = 16
$ListBox1.Location = New-Object System.Drawing.Point(87, 119)
$ListBox1.Size = New-Object System.Drawing.Size(174, 20)
$ListBox1.TabIndex = 3
$ListBox1.Items.AddRange([System.Object[]](@("Letzte Woche", "2 Wochen", "30 Tage", "2 Monate", "3 Monate")))
#-- Label3 --------------------------------------------------------------------
$Label3 = New-Object System.Windows.Forms.Label
$Label3.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Label3.Location = New-Object System.Drawing.Point(10, 75)
$Label3.Size = New-Object System.Drawing.Size(59, 17)
$Label3.TabIndex = 6
$Label3.Text = "Log"
#-- Logfile -------------------------------------------------------------------
$Logfile = New-Object System.Windows.Forms.ComboBox
$Logfile.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$Logfile.FormattingEnabled = $true
$Logfile.ItemHeight = 16
$Logfile.Location = New-Object System.Drawing.Point(87, 72)
$Logfile.Size = New-Object System.Drawing.Size(174, 20)
$Logfile.TabIndex = 2
$Logfile.Items.AddRange([System.Object[]](@("Application", "System", "Security")))
#-- InstanceID ----------------------------------------------------------------
$InstanceID = New-Object System.Windows.Forms.TextBox
$InstanceID.Font = New-Object System.Drawing.Font("Microsoft Sans Serif", 9.75, [System.Drawing.FontStyle]::Regular, [System.Drawing.GraphicsUnit]::Point, ([System.Byte](0)))
$InstanceID.Location = New-Object System.Drawing.Point(87, 29)
$InstanceID.Size = New-Object System.Drawing.Size(174, 22)
$InstanceID.TabIndex = 1
$InstanceID.Text = ""
$GroupBox1.Controls.Add($Button1)
$GroupBox1.Controls.Add($Label2)
$GroupBox1.Controls.Add($Label4)
$GroupBox1.Controls.Add($ListBox1)
$GroupBox1.Controls.Add($Label3)
$GroupBox1.Controls.Add($Logfile)
$GroupBox1.Controls.Add($InstanceID)
#-- Chart ---------------------------------------------------------------------
$Chart = New-Object System.Windows.Forms.PictureBox
$Chart.BackgroundImageLayout = [System.Windows.Forms.ImageLayout]::Stretch
$Chart.BorderStyle = [System.Windows.Forms.BorderStyle]::Fixed3D
$Chart.Location = New-Object System.Drawing.Point(343, 134)
$Chart.Size = New-Object System.Drawing.Size(716, 471)
$Chart.SizeMode = [System.Windows.Forms.PictureBoxSizeMode]::StretchImage
$Chart.TabIndex = 0
$Chart.TabStop = $false
$Chart.Text = ""
$Form1.Controls.Add($Label14)
$Form1.Controls.Add($Label15)
$Form1.Controls.Add($Label13)
$Form1.Controls.Add($Label12)
$Form1.Controls.Add($PictureBox1)
$Form1.Controls.Add($GroupBox3)
$Form1.Controls.Add($GroupBox2)
$Form1.Controls.Add($GroupBox1)
$Form1.Controls.Add($Chart)

function RunIt{
$lo = $logfile.value
write-host $lo
$chart.image = $null
$label1.text = "Last Generated: "
$label5.text = "Entry Type: "
$label6.text = "Source: "
$label7.text = "Event ID: "
$label8.text = "Instance ID: "
$label9.text = "User ID: "
$label10.text = "Message: "
$label11.text = "Additional Information: "
$ev = $instanceid.text
$xrow = 1  
$yrow = 2
$global:xl = New-Object -c excel.application 
$wb = $xl.workbooks.add() 
$sh = $wb.sheets.item(1) 
$q = 1
if($ListBox1.text -eq "Last 7 days"){$num = 7}
if($ListBox1.text -eq "Last 14 days"){$num = 14}
if($ListBox1.text -eq "Last 30 days"){$num = 30}
if($ListBox1.text -eq "Last 60 days"){$num = 60}
if($ListBox1.text -eq "Last 90 days"){$num = 90}
$event = Get-EventLog -LogName $logfile.text | where { $_.EventID -eq $ev} | select -First 1
$id = $event.instanceid
$label1.text += $event.timegenerated
$label5.text += $event.Entrytype
$label6.text += $event.source
$label7.text += $event.eventid
$label8.text += $event.instanceid
$label9.text += $event.username
$label10.text += $event.message
$label11.text += $event.ReplacementStrings
$events = Get-EventLog -LogName $logfile.text -instanceid $id -After ((Get-Date).Date.AddDays(-$num))| Group-Object {$_.TimeWritten.Date}
foreach ($stat in $events){
$Time = $stat.name
$Count = $stat.Count
$ti = Get-Date($Time)
$day = $ti.day
$month = $ti.month
$year = $ti.year
$sh.Cells.Item(1,$q) = "$day/$month/$year"
$sh.Cells.Item(2,$q) = $count
$q = $q +1
}
$range=$sh.UsedRange
$range.activate 

$ch = $xl.charts.add() 
$ch.chartstyle = 47
$ch.haslegend = $false
$ch.hastitle = $true
$ch.setSourceData($range)
$ch.ChartTitle.Caption = "EventLog $ev for Period of $num days"
$ch.HasDataTable = $true
$t = $ch.SeriesCollection("Series1")
$t.name = "Count" 
$fname = "Results"+$instanceid.text+".jpg"
$ch.export("$home\Documents\results$ev.jpg") 
$results = (Get-Item "$home\Documents\results$ev.jpg")
$chart.image = [System.Drawing.Image]::Fromfile($results)
#$label14.visible=$false
}


function Main{
	[System.Windows.Forms.Application]::EnableVisualStyles()
	[System.Windows.Forms.Application]::Run($Form1)
}


function PictureBox1Click( $object ){
$xl.speech.speak("Speech review functionality is not currently completed. Thank you.")
}

function Button1Click( $object ){
if ($instanceid.text -ne $null){RunIt}
write-host $logfile.text
}

Main 
