﻿#Model.ps1

#Bibliothek einbinden für Zugriff auf MySQL
[void][system.reflection.Assembly]::LoadFrom("c:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.3\Assemblies\v2.0\MySql.Data.dll");

#Verbindungszeichenfolge für Zugriff auf DB
#$connstring = "Server=10.161.8.17;Uid='root';Pwd='steinam';Database=dr_watson";
#Verbindungszeichenfolge für Zugriff auf DB
$connstring = "Server=localhost;Uid='root';Pwd='patricia1234';Database=drwatson";

#Connection-Objekt für MySQL 
$con = New-Object Mysql.Data.MysqlClient.MysqlConnection;
$con.ConnectionString = $connstring;

#Virtuelle Datenbank deklarieren
$DataSet = New-Object System.Data.DataSet

$SqlAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter
#$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;

function getCboAnwendung()
{
    $appname = @()  #leerer Array in ps
    $sqlQuery = "select Appname from tbl_report group by Appname"
    $SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
    $SqlCmd.CommandText = $SqlQuery
    $SqlCmd.Connection = $con
    
    verbindungOeffnen
    #Ausführen des SQL-Befehls
    #Zurückgeben eines .NET DAtaReader-Objektes
    $reader = $SQLCmd.ExecuteReader()

    #reader == assoziativer Array
    # kann nur einmal forward-only durchlaufen werden
    while($reader.Read())
    {
        $appname += $reader["Appname"]
    }
    verbindungschliessen

    return $appname #Array

}


function getCboRechner()
{
    $computer = @()  #leerer Array in ps
    $sqlQuery = "select Hostname from tbl_computer order by hostname"
    $SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
    $SqlCmd.CommandText = $SqlQuery
    $SqlCmd.Connection = $con
    
    verbindungOeffnen
    #Ausführen des SQL-Befehls
    #Zurückgeben eines .NET DAtaReader-Objektes
    $reader = $SQLCmd.ExecuteReader()

    #reader == assoziativer Array
    # kann nur einmal forward-only durchlaufen werden
    while($reader.Read())
    {
        $computer += $reader["Hostname"]
    }
    verbindungschliessen

    return $computer #Array

}



function getCboAnwender()
{

    $user = @()  #leerer Array in ps

    $sqlQuery = "select Anmeldename from tbl_user order by anmeldename"
    $SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
    $SqlCmd.CommandText = $SqlQuery
    $SqlCmd.Connection = $con

    
    verbindungOeffnen
    #Ausführen des SQL-Befehls
    #Zurückgeben eines .NET DAtaReader-Objektes
    $reader = $SQLCmd.ExecuteReader()

    #reader == assoziativer Array
    # kann nur einmal forward-only durchlaufen werden
    while($reader.Read())
    {
        $user += $reader["Anmeldename"]
    }

    verbindungschliessen

    return $user #Array

}



function getReports
{
    verbindungOeffnen
	#SQL-Statement eingeben
	$SqlQuery = "select * from tbl_report"

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$SqlAdapter.SelectCommand = $SqlCmd
	#Dataset leeren, sonst werden die alten Daten noch angezeigt
	$DataSet.Reset()
	#Dataset instantiieren und füllen lassen


	$SqlAdapter.Fill($DataSet)
	#Verbindung schließen
    verbindungschliessen
}



#Verbindung öffnen
function verbindungOeffnen()
{
	$con.Open();
	Write-Debug "Datenbankverbindung geöffnet"
}

#Verbindung schließen
function verbindungschliessen()
{
	$con.Close();
	Write-Debug "Datenbankverbindung geschlossen"
}