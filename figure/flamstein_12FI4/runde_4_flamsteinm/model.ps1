﻿#Model.ps1

#Bibliothek einbinden für Zugriff auf MySQL
[void][system.reflection.Assembly]::LoadFrom("c:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.3\Assemblies\v2.0\MySql.Data.dll");

#Verbindungszeichenfolge für Zugriff auf DB
$connstring = "Server=10.161.8.17;Uid='root';Pwd='steinam';Database=dr_watson";
$connstring = "Server=localhost;Uid='root';Pwd='patricia1234';Database=drwatson";

#Connection-Objekt für MySQL 
$con = New-Object Mysql.Data.MysqlClient.MysqlConnection;
$con.ConnectionString = $connstring;

#Virtuelle Datenbank deklarieren
$DataSet = New-Object System.Data.DataSet

$SqlAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter



function getCboModel ($comboBox)
{
	$SqlQuery = ""
	$FeldName = ""

	switch($comboBox)
	{
		"User" {
				  #SQL-Statement zum Befüllen der ComboBox cboUser	
				  $SqlQuery = "select Anmeldename from tbl_user;"
				  $Feldname = "Anmeldename"; 
				  break	
				 } 
				
		"Rechner" {
					#SQL-Staement zum Befüllen der ComboBox
					$SqlQuery = "select Hostname from tbl_computer;"
					$Feldname = "Hostname";
					break
					
					}	
					
				
		"Anwendung" {
					#SQL-Staement zum Befüllen der ComboBox
					$SqlQuery = "select AppName from tbl_report group by AppName;"
					$Feldname = "Appname";
					break
					
					}	
	}

	$list = @();
    verbindungOeffnen	

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$reader = $SqlCmd.ExecuteReader();
	
	while($reader.Read())
	{
		$list += $reader[$Feldname]
	}
    verbindungschliessen
	return $list
}





function getCboComputername
{
	$computer = @() # leerer Array
    
    $SqlQuery = "select Hostname from tbl_computer ORDER BY hostname"
    $SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con
    
    verbindungOeffnen
    
    # Ausführen des SQL-Befehls
    # Zurückgeben eines .NET DataReader-Objektes
    $reader = $SqlCmd.ExecuteReader()
    
    # $reader == assoziativer Array
    # kann nur einmal forward-only durchlaufen werden
    
    while($reader.Read())
    {
        $computer += $reader["Hostname"]
    }
    
    verbindungSchliessen
    
    return $computer
}

function getCboAnwendung
{
	$anwendung = @() # leerer Array
    
    $SqlQuery = "select Appname from tbl_report GROUP BY appname"
    $SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con
    
    verbindungOeffnen
    
    # Ausführen des SQL-Befehls
    # Zurückgeben eines .NET DataReader-Objektes
    $reader = $SqlCmd.ExecuteReader()
    
    # $reader == assoziativer Array
    # kann nur einmal forward-only durchlaufen werden
    
    while($reader.Read())
    {
        $anwendung += $reader["Appname"]
    }
    
    verbindungSchliessen
    
    return $anwendung
}

function getCboAnwender
{
	$user = @() # leerer Array
    
    $SqlQuery = "select Anmeldename from tbl_user"
    $SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con
    
    verbindungOeffnen
    
    # Ausführen des SQL-Befehls
    # Zurückgeben eines .NET DataReader-Objektes
    $reader = $SqlCmd.ExecuteReader()
    
    # $reader == assoziativer Array
    # kann nur einmal forward-only durchlaufen werden
    
    while($reader.Read())
    {
        $user += $reader["Anmeldename"]
    }
    
    verbindungSchliessen
    
    return $user
}

function getReportByComputername
{
    verbindungOeffnen
	#SQL-Statement eingeben
	$SqlQuery = "select * from tbl_report WHERE MAC = (select MAC from tbl_computer WHERE Hostname = '" + $cboComputername.SelectedItem + "')"

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$SqlAdapter.SelectCommand = $SqlCmd
	#Dataset leeren, sonst werden die alten Daten noch angezeigt
	$DataSet.Reset()
	#Dataset instantiieren und füllen lassen
	$SqlAdapter.Fill($DataSet)
	#Verbindung schließen
    verbindungschliessen
}

function getReportByAnwendung
{
    verbindungOeffnen
	#SQL-Statement eingeben
	$SqlQuery = "select * from tbl_report WHERE Appname = '" + $cboAnwendung.SelectedItem + "'"

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$SqlAdapter.SelectCommand = $SqlCmd
	#Dataset leeren, sonst werden die alten Daten noch angezeigt
	$DataSet.Reset()
	#Dataset instantiieren und füllen lassen
	$SqlAdapter.Fill($DataSet)
	#Verbindung schließen
    verbindungschliessen
}

function getReportByAnwender
{
    verbindungOeffnen
	#SQL-Statement eingeben
	$SqlQuery = "select * from tbl_report WHERE Anmeldename = '" + $cboAnmeldename.SelectedItem + "'"

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$SqlAdapter.SelectCommand = $SqlCmd
	#Dataset leeren, sonst werden die alten Daten noch angezeigt
	$DataSet.Reset()
	#Dataset instantiieren und füllen lassen
	$SqlAdapter.Fill($DataSet)
	#Verbindung schließen
    verbindungschliessen
}

function getReportsOld
{
    verbindungOeffnen
	#SQL-Statement eingeben
	$SqlQuery = "select * from tbl_report"

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$SqlAdapter.SelectCommand = $SqlCmd
	#Dataset leeren, sonst werden die alten Daten noch angezeigt
	$DataSet.Reset()
	#Dataset instantiieren und füllen lassen
	$SqlAdapter.Fill($DataSet)
	#Verbindung schließen
    verbindungschliessen
}


function getReports($Sql)
{
verbindungOeffnen
	#SQL-Statement eingeben
	$SqlQuery = $SQL

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$SqlAdapter.SelectCommand = $SqlCmd
	#Dataset leeren, sonst werden die alten Daten noch angezeigt
	$DataSet.Reset()
	#Dataset instantiieren und füllen lassen
	$SqlAdapter.Fill($DataSet)
	#Verbindung schließen
    verbindungschliessen

}




#Verbindung öffnen
function verbindungOeffnen()
{
	$con.Open();
	Write-Debug "Datenbankverbindung geöffnet"
}

#Verbindung schließen
function verbindungschliessen()
{
	$con.Close();
	Write-Debug "Datenbankverbindung geschlossen"
}

function updateDatenbankModel()
{
	verbindungOeffnen
	#CommandBuilder ist ein Objekt welches selbst SQL-Statements erstellen kann 
	#Er braucht lediglich das Wissen um den gewünschten SQLADAPTER 
	$commandbuilder = new-object MySql.Data.MySqlClient.MySqlCommandBuilder $SqlAdapter
	
	#Update/insert/delete-Statements werden vom CommandBuilder generiert und den jeweiligen
	#Befehlen des Adapters zugewiesen
	$SqlAdapter.UpdateCommand = $commandbuilder.GetUpdateCommand() 
	$SqlAdapter.InsertCommand = $commandbuilder.GetInsertCommand()
	$SqlAdapter.DeleteCommand = $commandbuilder.GetDeleteCommand() 

	$result = $SqlAdapter.Update($DataSet) 
	#$result	
	verbindungSchliessen
}

function exportToExcel
{
        $DataSet.Tables[0] | export-csv c:\temp\flam_stein\fileheute.csv -notypeinformation -UseCulture

}










