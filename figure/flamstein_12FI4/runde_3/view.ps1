﻿. .\control.ps1


#Generated Form Function
function GenerateForm {
########################################################################
# Code Generated By: SAPIEN Technologies PrimalForms (Community Edition) v1.0.10.0
# Generated On: 09.12.2014 08:49
# Generated By: Karl
########################################################################

#region Import the Assemblies
[reflection.assembly]::loadwithpartialname("System.Drawing") | Out-Null
[reflection.assembly]::loadwithpartialname("System.Windows.Forms") | Out-Null
#endregion

#region Generated Form Objects
$form1 = New-Object System.Windows.Forms.Form
$dataGrid1 = New-Object System.Windows.Forms.DataGrid
$btnDelete = New-Object System.Windows.Forms.Button
$btnUpdate = New-Object System.Windows.Forms.Button
$btnRechner = New-Object System.Windows.Forms.Button
$btnAnwendun = New-Object System.Windows.Forms.Button
$btnAnmeldename = New-Object System.Windows.Forms.Button
$btnDatum = New-Object System.Windows.Forms.Button
$monthCalendar = New-Object System.Windows.Forms.MonthCalendar
$btnExportReport = New-Object System.Windows.Forms.Button
$cboComputername = New-Object System.Windows.Forms.ComboBox
$lblComputername = New-Object System.Windows.Forms.Label
$cboAnwendung = New-Object System.Windows.Forms.ComboBox
$lblAnwendung = New-Object System.Windows.Forms.Label
$lblAnmeldename = New-Object System.Windows.Forms.Label
$cboAnmeldename = New-Object System.Windows.Forms.ComboBox
$InitialFormWindowState = New-Object System.Windows.Forms.FormWindowState
#endregion Generated Form Objects

#----------------------------------------------
#Generated Event Script Blocks
#----------------------------------------------
#Provide Custom Code for events specified in PrimalForms.
$handler_btnRechner_Click= 
{
    updateReportByComputername
}

$handler_btnDatum_Click= 
{
#TODO: Place custom script here

}

$handler_btnDelete_Click= 
{
#TODO: Place custom script here

}

$handler_btnUpdate_Click= 
{
  updateDatagrid

}

$handler_btnAnwendun_Click= 
{
    updateReportByAnwendung
}

$handler_btnExportReport_Click= 
{
   export2Excel

}

$handler_btnAnmeldename_Click= 
{
    updateReportByAnwender
}

$handler_form1_Load= 
{
    getDataGridData
    fillCboAnwender
    fillCboAnwendung
    fillCboComputername
}

$OnLoadForm_StateCorrection=
{#Correct the initial state of the form to prevent the .Net maximized form issue
	$form1.WindowState = $InitialFormWindowState
}

#----------------------------------------------
#region Generated Form Code
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 537
$System_Drawing_Size.Width = 636
$form1.ClientSize = $System_Drawing_Size
$form1.DataBindings.DefaultDataSourceUpdateMode = 0
$form1.Name = "form1"
$form1.Text = "Admin-Tool"
$form1.add_Load($handler_form1_Load)

$dataGrid1.DataBindings.DefaultDataSourceUpdateMode = 0
$dataGrid1.DataMember = ""
$dataGrid1.HeaderForeColor = [System.Drawing.Color]::FromArgb(255,0,0,0)
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 16
$System_Drawing_Point.Y = 241
$dataGrid1.Location = $System_Drawing_Point
$dataGrid1.Name = "dataGrid1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 235
$System_Drawing_Size.Width = 602
$dataGrid1.Size = $System_Drawing_Size
$dataGrid1.TabIndex = 23

$form1.Controls.Add($dataGrid1)


$btnDelete.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 148
$System_Drawing_Point.Y = 495
$btnDelete.Location = $System_Drawing_Point
$btnDelete.Name = "btnDelete"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 29
$System_Drawing_Size.Width = 93
$btnDelete.Size = $System_Drawing_Size
$btnDelete.TabIndex = 22
$btnDelete.Text = "Löschen"
$btnDelete.UseVisualStyleBackColor = $True
$btnDelete.add_Click($handler_btnDelete_Click)

$form1.Controls.Add($btnDelete)


$btnUpdate.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 16
$System_Drawing_Point.Y = 495
$btnUpdate.Location = $System_Drawing_Point
$btnUpdate.Name = "btnUpdate"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 29
$System_Drawing_Size.Width = 87
$btnUpdate.Size = $System_Drawing_Size
$btnUpdate.TabIndex = 21
$btnUpdate.Text = "Ändern"
$btnUpdate.UseVisualStyleBackColor = $True
$btnUpdate.add_Click($handler_btnUpdate_Click)

$form1.Controls.Add($btnUpdate)


$btnRechner.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 275
$System_Drawing_Point.Y = 115
$btnRechner.Location = $System_Drawing_Point
$btnRechner.Name = "btnRechner"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 84
$btnRechner.Size = $System_Drawing_Size
$btnRechner.TabIndex = 20
$btnRechner.Text = "Auswählen"
$btnRechner.UseVisualStyleBackColor = $True
$btnRechner.add_Click($handler_btnRechner_Click)

$form1.Controls.Add($btnRechner)


$btnAnwendun.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 160
$System_Drawing_Point.Y = 114
$btnAnwendun.Location = $System_Drawing_Point
$btnAnwendun.Name = "btnAnwendun"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 82
$btnAnwendun.Size = $System_Drawing_Size
$btnAnwendun.TabIndex = 19
$btnAnwendun.Text = "Auswählen"
$btnAnwendun.UseVisualStyleBackColor = $True
$btnAnwendun.add_Click($handler_btnAnwendun_Click)

$form1.Controls.Add($btnAnwendun)


$btnAnmeldename.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 30
$System_Drawing_Point.Y = 111
$btnAnmeldename.Location = $System_Drawing_Point
$btnAnmeldename.Name = "btnAnmeldename"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 98
$btnAnmeldename.Size = $System_Drawing_Size
$btnAnmeldename.TabIndex = 18
$btnAnmeldename.Text = "Auswählen"
$btnAnmeldename.UseVisualStyleBackColor = $True
$btnAnmeldename.add_Click($handler_btnAnmeldename_Click)

$form1.Controls.Add($btnAnmeldename)


$btnDatum.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 544
$System_Drawing_Point.Y = 201
$btnDatum.Location = $System_Drawing_Point
$btnDatum.Name = "btnDatum"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 35
$System_Drawing_Size.Width = 74
$btnDatum.Size = $System_Drawing_Size
$btnDatum.TabIndex = 17
$btnDatum.Text = "Auswählen"
$btnDatum.UseVisualStyleBackColor = $True
$btnDatum.add_Click($handler_btnDatum_Click)

$form1.Controls.Add($btnDatum)

$monthCalendar.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 440
$System_Drawing_Point.Y = 37
$monthCalendar.Location = $System_Drawing_Point
$monthCalendar.Name = "monthCalendar"
$monthCalendar.TabIndex = 16

$form1.Controls.Add($monthCalendar)


$btnExportReport.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 470
$System_Drawing_Point.Y = 495
$btnExportReport.Location = $System_Drawing_Point
$btnExportReport.Name = "btnExportReport"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 30
$System_Drawing_Size.Width = 154
$btnExportReport.Size = $System_Drawing_Size
$btnExportReport.TabIndex = 14
$btnExportReport.Text = "Reports exportieren"
$btnExportReport.UseVisualStyleBackColor = $True
$btnExportReport.add_Click($handler_btnExportReport_Click)

$form1.Controls.Add($btnExportReport)

$cboComputername.DataBindings.DefaultDataSourceUpdateMode = 0
$cboComputername.FormattingEnabled = $True
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 275
$System_Drawing_Point.Y = 63
$cboComputername.Location = $System_Drawing_Point
$cboComputername.Name = "cboComputername"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 84
$cboComputername.Size = $System_Drawing_Size
$cboComputername.TabIndex = 13

$form1.Controls.Add($cboComputername)

$lblComputername.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 275
$System_Drawing_Point.Y = 37
$lblComputername.Location = $System_Drawing_Point
$lblComputername.Name = "lblComputername"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblComputername.Size = $System_Drawing_Size
$lblComputername.TabIndex = 12
$lblComputername.Text = "Computername"

$form1.Controls.Add($lblComputername)

$cboAnwendung.DataBindings.DefaultDataSourceUpdateMode = 0
$cboAnwendung.FormattingEnabled = $True
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 160
$System_Drawing_Point.Y = 63
$cboAnwendung.Location = $System_Drawing_Point
$cboAnwendung.Name = "cboAnwendung"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 100
$cboAnwendung.Size = $System_Drawing_Size
$cboAnwendung.TabIndex = 11

$form1.Controls.Add($cboAnwendung)

$lblAnwendung.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 160
$System_Drawing_Point.Y = 37
$lblAnwendung.Location = $System_Drawing_Point
$lblAnwendung.Name = "lblAnwendung"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblAnwendung.Size = $System_Drawing_Size
$lblAnwendung.TabIndex = 10
$lblAnwendung.Text = "Anwendung"

$form1.Controls.Add($lblAnwendung)

$lblAnmeldename.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 28
$System_Drawing_Point.Y = 37
$lblAnmeldename.Location = $System_Drawing_Point
$lblAnmeldename.Name = "lblAnmeldename"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblAnmeldename.Size = $System_Drawing_Size
$lblAnmeldename.TabIndex = 9
$lblAnmeldename.Text = "Anmeldename"

$form1.Controls.Add($lblAnmeldename)

$cboAnmeldename.DataBindings.DefaultDataSourceUpdateMode = 0
$cboAnmeldename.FormattingEnabled = $True
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 28
$System_Drawing_Point.Y = 63
$cboAnmeldename.Location = $System_Drawing_Point
$cboAnmeldename.Name = "cboAnmeldename"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 100
$cboAnmeldename.Size = $System_Drawing_Size
$cboAnmeldename.TabIndex = 8

$form1.Controls.Add($cboAnmeldename)

#endregion Generated Form Code

#Save the initial state of the form
$InitialFormWindowState = $form1.WindowState
#Init the OnLoad event to correct the initial state of the form
$form1.add_Load($OnLoadForm_StateCorrection)
#Show the Form
$form1.ShowDialog()| Out-Null

} #End Function

#Call the Function
GenerateForm
