﻿Set-ExecutionPolicy Unrestricted
cls



function GetUsers
{

	return Get-ChildItem "C:\Users" | Select-Object Name
}





function GetWERPath($_user)
{


	$_ordner = "C:\Users\$_user\AppData\Local\Microsoft\Windows\WER\ReportArchive";
	
	[array]$werdata = $NULL;
	
	if((Test-Path -path $_ordner) -eq $true) # Testen ob es den Ordner gibt
	{
		$_ordner = Get-ChildItem $_ordner | Where-Object {$_.mode -match "d"} ;# Holt die Ordner aus dem Verzeichnis
		if($_ordner -ne $null) # Prüfen ob es unterordner gibt !!!!!!!!
		{
			foreach($_reportarch in $_ordner)
			{
				$_tempreportarch = $_reportarch.Name; # Namen wieder Temporär selektieren
				$_ordner2 = "C:\Users\$_user\AppData\Local\Microsoft\Windows\WER\ReportArchive\$_tempreportarch";
				if((Test-Path $_ordner2) -eq $true)# Testen ob es den Ordner gibt
				{
					$_unterordner = Get-ChildItem $_ordner2 | Where-Object {$_.name -eq "Report.wer"}; # nur Dateien welche Report.wer heisen werden angezeigt
					foreach($_error in $_unterordner)
					{
						$werdata += "C:\Users\$_user\AppData\Local\Microsoft\Windows\WER\ReportArchive\$_tempreportarch\$_error";
					}
				}
			}
		}
	}
	
	$_ordner = "C:\Users\$_user\AppData\Local\Microsoft\Windows\WER\ReportQueue";
	if((Test-Path $_ordner) -eq $true) # Testen ob es den Ordner gibt
	{
		$_ordner = Get-ChildItem $_ordner | Where-Object {$_.mode -match "d"};
		if($_ordner -ne $null) # Prüfen ob es unterordner gibt !!!!!!!!
		{
			foreach($_reportarch in $_ordner)
			{
				$_tempreportarch = $_reportarch.Name; # Namen wieder Temporär selektieren
				$_ordner2 = "C:\Users\$_user\AppData\Local\Microsoft\Windows\WER\ReportQueue\$_tempreportarch";
				if((Test-Path $_ordner2) -eq $true)# Testen ob es den Ordner gibt
				{
					$_unterordner = Get-ChildItem $_ordner2 | Where-Object {$_.name -eq "Report.wer"}; # nur Dateien welche Report.wer heisen werden angezeigt
					foreach($_error in $_unterordner)
					{
						$werdata += "C:\Users\$_user\AppData\Local\Microsoft\Windows\WER\ReportQueue\$_tempreportarch\$_error";
					}
				}
			}
		}
	}
	return $werdata;
}



function GetReportInnerData($_path,$_methode)
{




	$reportid= Select-String -Encoding Unicode -Path $_path -AllMatch "$_methode" | select line # sucht nach dem Stichpunkten welche in Methode übegeben werden im Path welcher über &_path kommt
	if ($reportid.Line -ne $null)
	{
		$reportidResult = $reportid.Line.Split("=");			 
	 	$reportid = $reportidResult[1];
		return $reportid;
	 	#### $reportid enthält die report ID 	
	}
	else
	{
        #Wa macht diese zeile ??
		$reportidResult = $reportid[1].Line.Split("=");
		$reportid = $reportidResult[1];
		return $reportid;
		#### $reportid enthält die report ID
	}
}



function GetReportData
{
    $stats=@()
	 
	$_users = GetUsers;
	foreach($_user in $_users) # $_users hier stehen alle benuter vom Rechner und werden nach $_user geschrieben in der Schleife
	{
        #Liste der WER-Dateien pro user
		$paths = GetWERPath $_user.Name;
		foreach($_path in $paths) # $_users hier stehen alle benuter vom Rechner und werden nach $_user geschrieben in der Schleife
		{
			if($_path -ne $null)
			{                                   #Pfad zur WER_Datei  u. gesuchter Key
				$_reportid = GetReportInnerData $_path "ReportIdentifier";
				$_reporttype = GetReportInnerData $_path "ReportType";
				$_eventtype = GetReportInnerData $_path "EventType"; # z.B AppCrash
				$_eventtime = GetReportInnerData $_path "EventTime";
				$_bucketid = GetReportInnerData $_path "Response.BucketId";
				$_appname = GetReportInnerData $_path "AppName"; 
				
                # Erzeugt aus Infos ein Objekt und legt es in Stats-Array ab
				$stats += New-Object PSObject -Property @{user=$_user.Name;report_id=$_reportid;report_type=$_reporttype;event_type=$_eventtype;event_time=$_eventtime;bucket_id=$_bucketid;app=$_appname};
			}
		}
	}
	
    return $stats	
	
}


function GetComputer
{
	$macadresse = get-wmiobject -class "Win32_NetworkAdapterConfiguration" | Where {$_.IpEnabled -Match "True"} | Select MacAddress
	
	$macadresse = $macadresse[0].MacAddress
	
	$os = (Get-WmiObject Win32_OperatingSystem).Name
	
	$comname = (Get-WmiObject Win32_OperatingSystem).CSName
	
	
}

Getreportdata

