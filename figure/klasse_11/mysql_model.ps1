﻿#mysql_model.ps1
cls
#Datenbankverbindung öffnen und Datenübertragung vorbereiten
#Bibliothek zur Datenbankeinbindung einbinden
[void][system.reflection.Assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.9\Assemblies\v4.0\MySql.Data.dll");

#Skriptweite Variablen deklarieren
#Connectionvariable
$connstring = "Server=localhost;Uid=watson;Pwd='watson';Database=watson_11FI3"
$con = New-Object Mysql.Data.MysqlClient.MysqlConnection;

#Commandobjekt
$command = New-Object MySql.Data.MySqlClient.MySqlCommand;


function open()
{
    #Funktion zum Öffnen der Datenbankverbindung
    try
    {
	    $script:con.ConnectionString = $connstring;
	    $con.Open();
	    Write-Debug "Datenbankverbindung geöffnet"
    }
    catch
    {
	    Write-Debug "Achtung Fehler: $_.ExceptionMessage"
	    Write-Debug "Daten müssen später übertragen werden"
    }
}


function close()
{
#Funktion zum Öffnen der Datenbankverbindung
    try
    {
	    $con.Close();
	    Write-Debug "Datenbankverbindung geschlossen"
    }
    catch
    {
	    Write-Debug "Achtung Fehler: $_.ExceptionMessage"
	    Write-Debug "Daten müssen später übertragen werden"
    }
}



function saveData([User]$Users)
{

    open

    [Computer]$pc = $Users[0].pc
    setComputer $pc
    
    foreach($u in $Users)
    {

        setUser $u


        setReports $u

    }

    close
}


function setUser([User]$u)
{
    #TODO: Has to be implemented
    $name = $u.Name


    [string]$sql = "replace into User(Anmeldename) values ('$name');"
    
    $command.CommandText = $sql
    $command.Connection = $con;


    open

        $command.ExecuteNonQuery()        

    close



}


function setReports([User]$u)
{

    $m = $u.pc.mac
    $uname  = $u.Name
 
    foreach($rep in $u.Reports)
    {
        $repid = $rep.ReportID
        $repType = $rep.ReportType
        $evTime = $rep.EventTime
        $evType = $rep.EventType
        $buckID = $rep.BucketID
        $app = $rep.Appname

        [string]$sql = "insert into report(ReportID, Appname, EventTime, BucketID, ReportType, User, Computer) "
        $sql += "values('$repid', '$app', '$evTime', '$buckID', '$repType', '$uname', '$m')"
      
        $command.CommandText = $sql
        $command.Connection = $con;

        $command.ExecuteNonQuery()

    }


    
}




function setComputer([Computer]$c)
{

    $m = $c.mac
    $sys = $c.OpSys
    $n = $c.Name


    [string]$sql = "replace into computer(MAC, OSName, HostName) values ('$m', '$sys','$n');"
    #[string]$sql = "Replace into computer values ($c.'mac', $c.'OpSys',$c.'Name');"
    
    $command.CommandText = $sql
    $command.Connection = $con;


    open

        $command.ExecuteNonQuery()        

    close

}



