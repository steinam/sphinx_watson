﻿. .\r3_model.ps1

function getReports
{
    getReportModel
    updateReport
}

function getCboUser()
{
    $UserList = getUserModel
    updateCboUser $UserList
}


function getCboHosts()
{
    $HostList = getHostModel
	updateCboHost $HostList
}



function updateReport()
{
	$dataGridWER.DataSource = $null;
	$dataGridWER.DataSource = $DataSet.Tables[0];
	$dataGridWER.DataBind
	$form1.refresh()
}


function updateCbouser($list)
{
    foreach($user in $list)
	{
		$cboUser.Items.Add($user)
	}
}

function updateCboHost($list)
{
    foreach($rechner in $list)
	{
		$cboRechner.Items.Add($rechner)
	}

}


function getReportsForUser($selectedUser)
{
	getReportsForUserModel $selectedUser
	updatereport

}

function getReportsForRechner($selectedRechner)
{
	getReportsForRechnerModel $selectedRechner
	updatereport

}