﻿$ErrorActionPreference = "SilentlyContinue"
$cpath = "C:\temp\wer_demo\"
$wer_objekte=@{} #nimmt alle ergebnisse auf

function get-object
{
    $wergruen = new-object -TypeName psobject -ArgumentList @{User="";App=""}

    $wergesell = [pscustomobject]@{User=""; App=""}

    $wer = New-Object -TypeName psObject
    Add-Member -InputObject $wer -Name User -Value "" -MemberType NoteProperty
    Add-Member -InputObject $wer -Name App -Value "" -MemberType NoteProperty
    #Add-Member -InputObject $wer -Name AppPath -Value "" -MemberType NoteProperty
    Add-Member -InputObject $wer -Name ReportType -Value "" -MemberType NoteProperty
    #Add-Member -InputObject $wer -Name MAC -Value "" -MemberType NoteProperty
    #Add-Member -InputObject $wer -Name DateOfWER -Value "" -MemberType NoteProperty
        
    return $wer

}

function get-Data($searchpattern, $filepath)
{

    $ergebnis = Select-String -pattern $searchpattern -exclude NsAppName -SimpleMatch -Encoding unicode -path $filepath
    $res = $ergebnis.Line
    $value = $res.Split("=")
    return $value[1]

}

$dateien = gci -Path  $cpath -Recurse

foreach($element in $dateien)
{
    if($element.psIsContainer)
    {
     #kein Interesse an den Ordnern
    }
    else
    {
        $o = get-object

        $o.App = Get-Data "AppName" $element.FullName
        $o.ReportType = get-Data "ReportType" $element.FullName  
        $o.User = $element.DirectoryName.Split("\")[3]

        $o | Export-Csv -Path C:\temp\wer7.csv -Append 
    }

}








