PS-Skript
-----------

Im vorliegenden Fall wurde die Daten der WER-Reports bereits in eine Datenbank überführt.
Das entsprechende Skript sieht wie folgt aus.

.. code-block:: bash

	cls
	#Datenbankverbindung öffnen und Datenübertragung vorbereiten
	#Bibliothek zur Datenbankeinbindung einbinden
	[void][system.reflection.Assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll");
	
	#Skriptweite Variablen deklarieren
	#Connectionvariable
	$connstring = "Server=localhost;Uid=root;Pwd='';Database=drwatson"
	$con = New-Object Mysql.Data.MysqlClient.MysqlConnection;
	
	#Commandobjekt
	$command = New-Object MySql.Data.MySqlClient.MySqlCommand;
	
	
	#Funktion zum Öffnen der Datenbankverbindung
	try
	{
		$script:con.ConnectionString = $connstring;
		$con.Open();
		Write-Host "Datenbankverbindung geöffnet"
	}
	catch
	{
		Write-Host "Achtung Fehler: $_.ExceptionMessage"
		Write-host "Daten müssen später übertragen werden"
	}
	
	
	
	#Funktionen
	#Funktion zum Überprüfen auf Vorhandensein von Daten der Tabelle Computer und ggf. einfügen
	function computerdatenGgfEinfuegen($zieltabelle, $kriterium, $wert)
	{
		#Prüfen, ob die Daten schon vorhanden sein
		$script:command.CommandText = "SELECT $kriterium FROM $zieltabelle WHERE $kriterium='$wert'"
		$script:command.Connection = $con
	
		$reader = $command.ExecuteReader();
		
		if($reader.HasRows -eq $true)
		{
			Write-Host "Datensatz bereits vorhanden"
			#Relevant, um weitere Abfragen durchführen zu können
			$reader.Close();
		}
		else
		{
			Write-Host "Datensatz noch nicht vorhanden"
			#Relevant, um weitere Abfragen durchführen zu können
			$reader.Close();
	
			#Weitere Daten für den Tabelleneintrag beschaffen
			#Zu beachten: Es ist hier die Methode Split zu wählen, da eine .NET Framework Objekt wohl geneniert wurde.
			#Die Stringoperation -split "|" scheitert an dieser Stelle
			$osname=  (Get-WmiObject Win32_OperatingSystem).name
			$osnamekurz=$osname.Split("|")
			$osname=$osnamekurz[0]
			$osname
	
			$hostname=(Get-WmiObject Win32_OperatingSystem).CSName
			$hostname
	
			#Einfügen des Datensatzes
			$command.CommandText = "Insert into $zieltabelle values ('$wert', '$osname', '$hostname')";
			$result=$command.ExecuteNonQuery();
			
			#Durchführung überprüfen, Rückgabwert 1 = erfolgreich, 0 = fehlerhaft
			if($result -eq 1)
			{
				Write-Host "Einfügen erfolgreich"
			}
			else
			{
				Write-Host "Leider ein Fehler aufgetreten"
			}
		}
	}
	
	#Funktion zum Überprüfen auf Vorhandensein von Daten allgemein und ggf. einfügen
	function datenGgfEinfuegen($zieltabelle, $kriterium, $wert)
	{
		#Prüfen, ob die Daten schon vorhanden sein
		$script:command.CommandText = "SELECT $kriterium FROM $zieltabelle WHERE $kriterium='$wert'"
		$script:command.Connection = $con
	
		$reader = $command.ExecuteReader();
		
		if($reader.HasRows -eq $true)
		{
			Write-Host "Datensatz bereits vorhanden"
			#Relevant, um weitere Abfragen durchführen zu können
			$reader.Close();
		
		}
		else
		{
			Write-Host "Datensatz noch nicht vorhanden"
			#Relevant, um weitere Abfragen durchführen zu können
			$reader.Close();
			
			#Einfügen des Datensatzes
			$command.CommandText = "Insert into $zieltabelle($kriterium) values ('$wert')";
			$result=$command.ExecuteNonQuery();
			
			#Durchführung überprüfen, Rückgabwert 1 = erfolgreich, 0 = fehlerhaft
			if($result -eq 1)
			{
				Write-Host "Einfügen erfolgreich"
			}
			else
			{
				Write-Host "Leider ein Fehler aufgetreten"
			}
		
		}
		#Relevant, um weitere Abfragen durchführen zu können
		$reader.Close();
	}
	
	#Aufgrund des Schülerbeispiels wird hier nur auf einen Ordner zurückgegriffen, des jeweils angemeldeten Users
	#In der Praxis müsste auf alle User des PCs zugegriffen werden. Folgender Hinweis ist hierbei zu beachten:
	#http://www.powershell-group.eu/powershell-windows-error-reporting-wer/
	$pfad="$env:USERPROFILE\AppData\Local\Microsoft\Windows\WER\ReportArchive"
	$werordner = get-childitem $pfad
	
	
	#Benötigte Daten:
	#Allgemeine Details
	
	#Allgemeine Daten sammeln
	#Wertvolle Infos aus der entsprechenden Klasse: http://msdn.microsoft.com/en-us/library/aa394239(v=vs.85).aspx
	$macAdresse = get-wmiobject -class "Win32_NetworkAdapterConfiguration" | Where {$_.IpEnabled -Match "True"} | Select MacAddress
	$macAdresse=$macAdresse.MacAddress
	computerdatenGgfEinfuegen  "tbl_Computer" "MAC" $macAdresse
	
	#Weitere Details, Vorname und Nachnam bleiben erstmal in der DB unausgefüllt, wäre ggf. später
	#über eine AD-Abfrage noch zu realisieren
	$user = [Environment]::UserName
	$user
	datenGgfEinfuegen "tbl_user" "Anmeldename" $user
	
	
	#Reportdetails ermitteln und an die Datenbank übertragen
	if ($con.State -eq "Open")
	{
		#Reportdetails auslesen und übermitteln 
		foreach($ordner in $werordner)
		{
			#Pfad etwas tricky zusammenzubauen
			$reportid=Select-String -Encoding Unicode  -path  "$pfad\$ordner\Report.wer" -AllMatch "ReportIdentifier" | select line 
			if ($reportid.Line -ne $null)
			{
				$reportidResult=$reportid.Line.Split("=")
				$reportid=$reportidResult[1]
				$reportid
			}
			else 
			{
				Write-Host "ReportId aufgrund Arraystruktur besonders erzeugen"
				$reportidResult=$reportid[1].Line.Split("=")
				$reportid=$reportidResult[1]
				$reportid
			}
			
			#Überprüfen, ob der Report aufgrund der ReportId schon in der DB eingetragen wurde
			
			$script:command.CommandText = "SELECT ReportID FROM tbl_report WHERE reportid='$reportid'"
			$script:command.Connection = $con
	
			$reader = $command.ExecuteReader();
		
			if($reader.HasRows -eq $true)
			{
				Write-Host "Datensatz bereits vorhanden"
				#Relevant, um weitere Abfragen durchführen zu können
				$reader.Close();
			}
			else
			{ 
				Write-Host "Report noch nicht vorhanden, wird nun an die DB übertragen"
				$reader.Close();
				#Report-Type: Nicht jeder Report hat einen Report-type, sofern keiner gegeben, wird einer auf der Powershellschicht geschaffen
				#hier 99 für Non Critical
				$reporttype=Select-String -Encoding Unicode  -path  "$pfad\$ordner\Report.wer" -AllMatch "ReportType" | select line 
				if ($reporttype.Line -ne $null)
				{
					$reporttypeResult=$reporttype.Line.Split("=")
					$reportType=$reporttypeResult[1]
					$reporttype
					datenGgfEinfuegen "tbl_reporttype" "Reporttype" $reporttype		
				}
				else
				{
					$reporttype=99
					$reportType
					datenGgfEinfuegen "tbl_reporttype" "Reporttype" $reporttype		
				}
				
				$eventTime=Select-String -Encoding Unicode  -path  "$pfad\$ordner\Report.wer" -SimpleMatch "EventTime" | select line 
				$eventTimeResult=$eventTime.Line.Split("=")
				$eventTime=$eventTimeResult[1]
				$eventTime
				
				$bucketid=Select-String -Encoding Unicode -path "$pfad\$ordner\Report.wer" -SimpleMatch "Response.BucketID" | select line
				$bucketidResult=$bucketid.Line.Split("=")
				$bucketid=$bucketidresult[1]
				$bucketid
				
				$AppName=Select-String -Encoding Unicode -path "$pfad\$ordner\Report.wer" -SimpleMatch "AppName" | select line
				$AppNameResult=$AppName.Line.Split("=")
				$AppName=$AppNameresult[1]
				$AppName
				
				#Daten für die Fremdschlüssel in Reporttabelle nochmals beschaffen
				$user = [Environment]::UserName
	
				$macAdresse = get-wmiobject -class "Win32_NetworkAdapterConfiguration" | Where {$_.IpEnabled -Match "True"} | Select MacAddress
				$macAdresse=$macAdresse.MacAddress
				
				#Reportdaten an Datenbank übermitteln
				#Noch anpassen
				$command.CommandText = "Insert into tbl_report values ('$reportid', '$AppName', '$eventTime', '$bucketid', '$user', '$macadresse', '$reportType')";
				$result=$command.ExecuteNonQuery();
				
				#Durchführung überprüfen, Rückgabwert 1 = erfolgreich, 0 = fehlerhaft
				if($result -eq 1)
				{
					Write-Host "Einfügen erfolgreich"
				}
				else
				{
					Write-Host "Leider ein Fehler aufgetreten"
				}
			}
		}
		#Verbindung zum Schluss des Skriptes schließen
		$con.Close();
		Write-Host "Datenbankverbindung geschlossen"
	}


