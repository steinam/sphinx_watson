drop database if exists drwatson ;
create database drwatson;
use drwatson; 

create table tbl_Computer
(
    MAC varchar(60),
    OSName varchar(60),
    Hostname varchar(60),
    primary key (MAC)
)ENGINE=InnoDB;

create table tbl_User
(
    Anmeldename varchar(60),
    Vorname varchar(30),
    Nachname varchar(30),
    primary key (Anmeldename)
)ENGINE=InnoDB;

create table tbl_Reporttype
(
    Reporttype int(10),
    Beschreibung varchar(60),
    primary key (Reporttype)
)ENGINE=InnoDB;


create table tbl_report
(
    ReportID  varchar(60)  not null,
    Appname varchar(60),
    Eventtime int(60),
    BucketID int(60),
    primary key (ReportID),
    Anmeldename varchar(60),
    MAC varchar(60),
    Reporttype int(10)
) ENGINE=InnoDB;


Alter table tbl_report
    ADD FOREIGN KEY (Anmeldename) REFERENCES tbl_User(Anmeldename),
    ADD FOREIGN KEY (MAC) references tbl_Computer(MAC),
    ADD FOREIGN KEY (Reporttype) references tbl_Reporttype(Reporttype);
    