-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 12. Aug 2014 um 20:25
-- Server Version: 5.5.16
-- PHP-Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `drwatson`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_computer`
--

CREATE TABLE IF NOT EXISTS `tbl_computer` (
  `MAC` varchar(60) NOT NULL DEFAULT '',
  `OSName` varchar(60) DEFAULT NULL,
  `Hostname` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`MAC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_computer`
--

INSERT INTO `tbl_computer` (`MAC`, `OSName`, `Hostname`) VALUES
('A0:88:B4:99:C9:F0', 'Microsoft Windows 7 Professional ', 'C-THINK');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_report`
--

CREATE TABLE IF NOT EXISTS `tbl_report` (
  `ReportID` varchar(60) NOT NULL,
  `Appname` varchar(60) DEFAULT NULL,
  `Eventtime` int(60) DEFAULT NULL,
  `BucketID` int(60) DEFAULT NULL,
  `Anmeldename` varchar(60) DEFAULT NULL,
  `MAC` varchar(60) DEFAULT NULL,
  `Reporttype` int(10) DEFAULT NULL,
  PRIMARY KEY (`ReportID`),
  KEY `Anmeldename` (`Anmeldename`),
  KEY `MAC` (`MAC`),
  KEY `Reporttype` (`Reporttype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_report`
--

INSERT INTO `tbl_report` (`ReportID`, `Appname`, `Eventtime`, `BucketID`, `Anmeldename`, `MAC`, `Reporttype`) VALUES
('0496bde8-8d9f-11e3-85c8-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1365921976, 'C', 'A0:88:B4:99:C9:F0', 99),
('07016bd5-5bf7-11e3-ad08-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1666951804, 'C', 'A0:88:B4:99:C9:F0', 99),
('08bf7888-eb2b-11e0-86f0-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267225681, 'C', 'A0:88:B4:99:C9:F0', 99),
('0bbb9240-eb2b-11e0-86f0-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267224891, 'C', 'A0:88:B4:99:C9:F0', 99),
('0c36e563-39f3-11e2-bc8a-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1302581026, 'C', 'A0:88:B4:99:C9:F0', 99),
('0c90d4d3-b18d-11e2-9739-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 2147483647, 'C', 'A0:88:B4:99:C9:F0', 99),
('0eb7abf8-eb2b-11e0-86f0-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267225006, 'C', 'A0:88:B4:99:C9:F0', 99),
('0f340180-da98-11e3-a331-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('11b3c5b0-eb2b-11e0-86f0-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267225612, 'C', 'A0:88:B4:99:C9:F0', 99),
('1729488d-2394-11e3-b1cd-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132679, 'C', 'A0:88:B4:99:C9:F0', 99),
('17ca3aeb-d349-11e2-b0fb-b51b1c98e6f4', 'USB-Massenspeichergerät', 2147483647, 2147483647, 'C', 'A0:88:B4:99:C9:F0', 99),
('1955a11a-beec-11e3-a33e-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1223982013, 'C', 'A0:88:B4:99:C9:F0', 99),
('1a6ff455-2394-11e3-b1cd-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132809, 'C', 'A0:88:B4:99:C9:F0', 99),
('1c6548aa-39f3-11e2-bc8a-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1233576197, 'C', 'A0:88:B4:99:C9:F0', 99),
('1de9799c-9c63-11e3-9d9e-f0def1706fca', 'Unknown Device', 2147483647, 388111150, 'C', 'A0:88:B4:99:C9:F0', 99),
('205ba629-bd3b-11e0-8d12-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1138132679, 'C', 'A0:88:B4:99:C9:F0', 99),
('2337f348-bd3b-11e0-8d12-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1138132809, 'C', 'A0:88:B4:99:C9:F0', 99),
('248180e5-e8cd-11e0-ad39-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267225681, 'C', 'A0:88:B4:99:C9:F0', 99),
('277d9a9c-e8cd-11e0-ad39-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267224891, 'C', 'A0:88:B4:99:C9:F0', 99),
('2a79b454-e8cd-11e0-ad39-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267225612, 'C', 'A0:88:B4:99:C9:F0', 99),
('2bd19d5a-bd3b-11e0-8d12-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1138132529, 'C', 'A0:88:B4:99:C9:F0', 99),
('2d782f6d-e8cd-11e0-ad39-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1267225006, 'C', 'A0:88:B4:99:C9:F0', 99),
('32cdfdc3-035f-11e4-9c85-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('3418e16e-39f1-11e2-bc8a-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1233576197, 'C', 'A0:88:B4:99:C9:F0', 99),
('343a9e5b-fbe0-11e3-b525-f0def1706fca', 'WindowsExplorer', 2147483647, 55769560, 'C', 'A0:88:B4:99:C9:F0', 3),
('3d207e7a-e59b-11e3-a724-f0def1706fca', 'DAVINCI LOOK 6 Programmdatei', 2147483647, 210328511, 'C', 'A0:88:B4:99:C9:F0', 2),
('4117dade-5b58-11e3-8c9a-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1045973700, 'C', 'A0:88:B4:99:C9:F0', 99),
('41934ab6-d6b6-11e3-8790-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('4e4c8cd4-deb2-11e0-986c-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1178946239, 'C', 'A0:88:B4:99:C9:F0', 99),
('5171ba2f-deb2-11e0-986c-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1178946239, 'C', 'A0:88:B4:99:C9:F0', 99),
('53c500d3-e994-11e0-841e-028037ec0200', 'Microsoft Visio', 2147483647, 1130529712, 'C', 'A0:88:B4:99:C9:F0', 99),
('5d4fba9a-020b-11e4-9eb7-f0def1706fca', 'PowerGUI', 2147483647, 2147483647, 'C', 'A0:88:B4:99:C9:F0', 2),
('5f4303b3-f345-11e0-8ec8-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 462660546, 'C', 'A0:88:B4:99:C9:F0', 99),
('60d549dd-beec-11e3-a33e-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1223982013, 'C', 'A0:88:B4:99:C9:F0', 99),
('621f58f6-11f0-11e2-b883-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1666951804, 'C', 'A0:88:B4:99:C9:F0', 99),
('64bd778c-8cdc-11e3-8317-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1019114223, 'C', 'A0:88:B4:99:C9:F0', 99),
('679202a6-d1e7-11e3-9ebb-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('67d366db-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039247, 'C', 'A0:88:B4:99:C9:F0', 99),
('683436fb-f8aa-11e0-8f27-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 776953355, 'C', 'A0:88:B4:99:C9:F0', 99),
('6a8abfc1-51c1-11e2-8c8d-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132679, 'C', 'A0:88:B4:99:C9:F0', 99),
('6ab5d11a-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039622, 'C', 'A0:88:B4:99:C9:F0', 99),
('6ab5d11b-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199038716, 'C', 'A0:88:B4:99:C9:F0', 99),
('6ab5d11c-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1260680720, 'C', 'A0:88:B4:99:C9:F0', 99),
('6c8bb83f-8cdc-11e3-8317-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 852311529, 'C', 'A0:88:B4:99:C9:F0', 99),
('6c8bb840-8cdc-11e3-8317-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1019114283, 'C', 'A0:88:B4:99:C9:F0', 99),
('6d6f25d8-51c1-11e2-8c8d-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132809, 'C', 'A0:88:B4:99:C9:F0', 99),
('6ed2903b-6839-11e1-aae5-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1159221087, 'C', 'A0:88:B4:99:C9:F0', 99),
('6f193118-f706-11e0-8f92-028037ec0200', 'Assistent für Problembehandlung per Diagnose', 2147483647, 452704182, 'C', 'A0:88:B4:99:C9:F0', 99),
('706b8119-51c1-11e2-8c8d-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132529, 'C', 'A0:88:B4:99:C9:F0', 99),
('73c4aa3a-bada-11e0-a48f-cc52afe2d81f', 'Gerätetreiberinstallation', 2147483647, 1801173967, 'C', 'A0:88:B4:99:C9:F0', 99),
('75df0931-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039094, 'C', 'A0:88:B4:99:C9:F0', 99),
('75df0932-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039399, 'C', 'A0:88:B4:99:C9:F0', 99),
('78c02842-0882-11e2-bd08-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1302581026, 'C', 'A0:88:B4:99:C9:F0', 99),
('7c20254c-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039247, 'C', 'A0:88:B4:99:C9:F0', 99),
('7c753a81-2122-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1260680845, 'C', 'A0:88:B4:99:C9:F0', 99),
('7ff1ffef-eb4b-11e0-86f0-028037ec0200', 'Assistent für Problembehandlung per Diagnose', 2147483647, 2147483647, 'C', 'A0:88:B4:99:C9:F0', 99),
('86e76bd1-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1260680720, 'C', 'A0:88:B4:99:C9:F0', 99),
('875e1e30-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039622, 'C', 'A0:88:B4:99:C9:F0', 99),
('875e1e31-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199038716, 'C', 'A0:88:B4:99:C9:F0', 99),
('88fad74e-d765-11e3-b612-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('89e2406c-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039094, 'C', 'A0:88:B4:99:C9:F0', 99),
('89e2406d-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039399, 'C', 'A0:88:B4:99:C9:F0', 99),
('89e2406e-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1260680845, 'C', 'A0:88:B4:99:C9:F0', 99),
('90ab424b-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039247, 'C', 'A0:88:B4:99:C9:F0', 99),
('90ab424c-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039622, 'C', 'A0:88:B4:99:C9:F0', 99),
('90ab424d-20f2-11e3-9ea4-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199038716, 'C', 'A0:88:B4:99:C9:F0', 99),
('91b4bb4d-2393-11e3-b1cd-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132529, 'C', 'A0:88:B4:99:C9:F0', 99),
('93df3ac1-25a8-11e3-b734-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1321711379, 'C', 'A0:88:B4:99:C9:F0', 99),
('95ecd5c0-e806-11e0-9a6a-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1178946239, 'C', 'A0:88:B4:99:C9:F0', 99),
('962d018c-d69b-11e3-8790-f0def1706fca', 'Assistent für Problembehandlung per Diagnose', 2147483647, 1068450269, 'C', 'A0:88:B4:99:C9:F0', 99),
('98b50222-e008-11e3-bfd8-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('a0ca0efb-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1260680720, 'C', 'A0:88:B4:99:C9:F0', 99),
('a0ca0efc-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1260680845, 'C', 'A0:88:B4:99:C9:F0', 99),
('a0ca0efd-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039094, 'C', 'A0:88:B4:99:C9:F0', 99),
('a77124d8-dc16-11e3-a1d1-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('a83323c2-1ec8-11e3-ba96-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1199039399, 'C', 'A0:88:B4:99:C9:F0', 99),
('af2d814d-5331-11e1-bcca-028037ec0200', 'Unknown Device', 2147483647, 388111150, 'C', 'A0:88:B4:99:C9:F0', 99),
('b6bc34b0-dd29-11e0-a860-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1321711379, 'C', 'A0:88:B4:99:C9:F0', 99),
('bb2dfe87-e995-11e0-841e-028037ec0200', 'Microsoft Visio', 2147483647, 1130529712, 'C', 'A0:88:B4:99:C9:F0', 99),
('bbdb667f-7740-11e1-a6eb-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1302581026, 'C', 'A0:88:B4:99:C9:F0', 99),
('c62bf528-de42-11e2-b8a6-f04e14c73829', 'Gerätetreiberinstallation', 2147483647, 1551783018, 'C', 'A0:88:B4:99:C9:F0', 99),
('ca646141-dded-11e0-b9c8-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1135371291, 'C', 'A0:88:B4:99:C9:F0', 99),
('cf75d69c-dea0-11e0-986c-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 1321711379, 'C', 'A0:88:B4:99:C9:F0', 99),
('cfe3696f-2d8c-11e2-910a-f0def1706fca', 'Assistent für Problembehandlung per Diagnose', 2147483647, 452704182, 'C', 'A0:88:B4:99:C9:F0', 99),
('d05f30e5-de42-11e2-b8a6-f04e14c73829', 'Gerätetreiberinstallation', 2147483647, 1551783018, 'C', 'A0:88:B4:99:C9:F0', 99),
('d2fe94dc-49a8-11e2-b0e1-f0def1706fca', 'Unknown Device', 2147483647, 388111150, 'C', 'A0:88:B4:99:C9:F0', 99),
('d58782ad-8ae0-11e1-8763-028037ec0200', 'Assistent für Problembehandlung per Diagnose', 2147483647, 452704182, 'C', 'A0:88:B4:99:C9:F0', 99),
('d61d21ad-c5d0-11e2-95f7-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132679, 'C', 'A0:88:B4:99:C9:F0', 99),
('d90449e4-d431-11e3-8446-f0def1706fca', 'Windows-Explorer', 2147483647, 80940756, 'C', 'A0:88:B4:99:C9:F0', 2),
('d91229d3-c5d0-11e2-95f7-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132809, 'C', 'A0:88:B4:99:C9:F0', 99),
('daa5af48-ea26-11e3-9c0a-f0def1706fca', 'MySQL Workbench', 2147483647, 2147483647, 'C', 'A0:88:B4:99:C9:F0', 2),
('dbff8091-faac-11e3-a2a8-f0def1706fca', 'Müller Foto.exe', 2147483647, 19486805, 'C', 'A0:88:B4:99:C9:F0', 2),
('df7498f6-3259-11e3-b63c-f0def1706fca', 'USB-Massenspeichergerät', 2147483647, 1914225191, 'C', 'A0:88:B4:99:C9:F0', 99),
('e100fe0d-d4e8-11e3-a010-f0def1706fca', 'WindowsExplorer', 2147483647, 54507390, 'C', 'A0:88:B4:99:C9:F0', 3),
('e4be8583-9a80-11e1-ae14-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 772937264, 'C', 'A0:88:B4:99:C9:F0', 99),
('e4cd736c-2955-11e1-be12-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 5353263, 'C', 'A0:88:B4:99:C9:F0', 99),
('e5ff4f9c-031a-11e1-8041-028037ec0200', 'Bing Client Extensions', 2147483647, 2078954956, 'C', 'A0:88:B4:99:C9:F0', 99),
('e97420fc-c5d0-11e2-95f7-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1138132529, 'C', 'A0:88:B4:99:C9:F0', 99),
('ed3703e7-be81-11e0-91b9-028037ec0200', 'Gerätetreiberinstallation', 2147483647, 4592850, 'C', 'A0:88:B4:99:C9:F0', 99),
('f93e8cce-11ee-11e2-b883-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1267224891, 'C', 'A0:88:B4:99:C9:F0', 99),
('fc3c5bfb-11ee-11e2-b883-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1267225006, 'C', 'A0:88:B4:99:C9:F0', 99),
('fc3c5bfc-11ee-11e2-b883-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1267225612, 'C', 'A0:88:B4:99:C9:F0', 99),
('fdeb8f80-4dc4-11e3-91de-f0def1706fca', 'Unknown Device', 2147483647, 388111150, 'C', 'A0:88:B4:99:C9:F0', 99),
('fe3c8509-8d9e-11e3-85c8-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1365923196, 'C', 'A0:88:B4:99:C9:F0', 99),
('fe3c850a-8d9e-11e3-85c8-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1365923544, 'C', 'A0:88:B4:99:C9:F0', 99),
('fe3c850b-8d9e-11e3-85c8-f0def1706fca', 'Gerätetreiberinstallation', 2147483647, 1365923646, 'C', 'A0:88:B4:99:C9:F0', 99);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_reporttype`
--

CREATE TABLE IF NOT EXISTS `tbl_reporttype` (
  `Reporttype` int(10) NOT NULL DEFAULT '0',
  `Beschreibung` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`Reporttype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_reporttype`
--

INSERT INTO `tbl_reporttype` (`Reporttype`, `Beschreibung`) VALUES
(2, NULL),
(3, NULL),
(99, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `Anmeldename` varchar(60) NOT NULL DEFAULT '',
  `Vorname` varchar(30) DEFAULT NULL,
  `Nachname` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Anmeldename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_user`
--

INSERT INTO `tbl_user` (`Anmeldename`, `Vorname`, `Nachname`) VALUES
('C', NULL, NULL);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_report`
--
ALTER TABLE `tbl_report`
  ADD CONSTRAINT `tbl_report_ibfk_1` FOREIGN KEY (`Anmeldename`) REFERENCES `tbl_user` (`Anmeldename`),
  ADD CONSTRAINT `tbl_report_ibfk_2` FOREIGN KEY (`MAC`) REFERENCES `tbl_computer` (`MAC`),
  ADD CONSTRAINT `tbl_report_ibfk_3` FOREIGN KEY (`Reporttype`) REFERENCES `tbl_reporttype` (`Reporttype`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
